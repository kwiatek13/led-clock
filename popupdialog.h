#ifndef POPUPDIALOG_H
#define POPUPDIALOG_H

#include <QDialog>


namespace Ui {
class PopupDialog;
}

class PopupDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PopupDialog(QWidget *parent = 0);
    ~PopupDialog();
    void SetAsAcceptWindow(QString Tx);
    void SetAsAcceptWindowWithReturn(QString Tx, QString ReturnValue);
    void SetAsSelectOptionWindow(QString Tx, QStringList List, bool IndexReturn=false);
    void SetAsEnterWindow(QString Tx);
    void SetAsEnterWindowWith2Args(QString Tx, QString Tx2);

private:
    Ui::PopupDialog *ui;
    QString Text;
    QStringList OptionsList;
    QString RetVal;
    bool IsIndexReturn=false;

signals:
    void ReturnValue(QString Val);
private slots:
    void on_OkButton_clicked();

    void on_EnterEdit_editingFinished();
    void on_List_clicked(const QModelIndex &index);
    void on_CancelButton_clicked();
    void on_EnterEdit2_editingFinished();
};

#endif // POPUPDIALOG_H
