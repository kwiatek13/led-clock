#include "customalbumrow.h"

CustomAlbumRow::CustomAlbumRow(QWidget *parent, int Mode) : QWidget(parent)
{
    //  setStyleSheet("border: 0px solid white;border-radius: 10px;background-color: rgb(212,212,212);");
//    HBLayout.addWidget(&NameLabel);
//    FavouriteButton.setMaximumHeight(30);
//    FavouriteButton.setMaximumWidth(30);
//    FavouriteButton.setMinimumHeight(30);
//    FavouriteButton.setMinimumWidth(30);

 //   FavouriteButton.setStyleSheet("border: 0px solid white;border-radius: 7px;background-color: rgb(212,212,212);");

    if(Mode == PlaylistMode)
    {
        HBLayout.addWidget(&NameLabel);
        FavouriteButton = new QPushButton;
        FavouriteButton->setMaximumHeight(30);
        FavouriteButton->setMaximumWidth(30);
        FavouriteButton->setMinimumHeight(30);
        FavouriteButton->setMinimumWidth(30);

        FavouriteButton->setStyleSheet("border: 0px solid white;border-radius: 7px;background-color: rgb(212,212,212);");

        FavouriteButton->setText("Del");
        HBLayout.addWidget(FavouriteButton);
    }
    else if(Mode == AlbumMode)
    {
        HBLayout.addWidget(&NameLabel);
        FavouriteButton = new QPushButton;
        FavouriteButton->setMaximumHeight(30);
        FavouriteButton->setMaximumWidth(30);
        FavouriteButton->setMinimumHeight(30);
        FavouriteButton->setMinimumWidth(30);

        FavouriteButton->setStyleSheet("border: 0px solid white;border-radius: 7px;background-color: rgb(212,212,212);");
        FavouriteButton->setText("Fav");
        PlaylistButton = new QPushButton;
        PlaylistButton->setMaximumHeight(30);
        PlaylistButton->setMaximumWidth(30);
        PlaylistButton->setMinimumHeight(30);
        PlaylistButton->setMinimumWidth(30);
        PlaylistButton->setText("Pla");
        PlaylistButton->setStyleSheet("border: 0px solid white;border-radius: 7px;background-color: rgb(212,212,212);");
        HBLayout.addWidget(PlaylistButton);
        connect(FavouriteButton, SIGNAL(clicked()), this, SLOT(SetColorToFavButton()));
        HBLayout.addWidget(FavouriteButton);
    }
    else if(Mode == FileManagerMode)
    {
        CheckBoxPtr = new QCheckBox;
        CheckBoxPtr->setMaximumHeight(50);
        CheckBoxPtr->setMaximumWidth(50);
        CheckBoxPtr->setMinimumHeight(50);
        CheckBoxPtr->setMinimumWidth(50);
        CheckBoxPtr->setStyleSheet("QCheckBox::indicator {width: 25px; height: 25px;}");
        HBLayout.addWidget(CheckBoxPtr);
        HBLayout.addWidget(&NameLabel);
    }

    NameLabel.setMaximumWidth(170);


    //HBLayout.addWidget(&FavouriteButton);

    setLayout(&HBLayout);

}

CustomAlbumRow::~CustomAlbumRow()
{
    if(FavouriteButton) delete FavouriteButton;
    if(PlaylistButton) delete PlaylistButton;
    if(CheckBoxPtr) delete CheckBoxPtr;
  //  qDebug () <<"Destroy CustomAlbumRow \n";
}

void CustomAlbumRow::SetLabel(QString Text)
{
    NameLabel.setText(Text);
}

void CustomAlbumRow::SetColorToFavButton()
{
    FavouriteButton->setStyleSheet("border: 0px solid white;border-radius: 7px;background-color: rgb(33,194,76);");

}
