#include "albumslibrary.h"
#include "ui_albumslibrary.h"

AlbumsLibrary::AlbumsLibrary(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AlbumsLibrary)
{
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(0);
    ui->NextPage->setVisible(false);
    ui->PrevPage->setVisible(false);
    ui->SelectAlarmLabel->setVisible(false);
    connect(&Popup, SIGNAL(ReturnValue(QString)), this, SLOT(AddToSelectedPlaylist(QString)));
}

AlbumsLibrary::~AlbumsLibrary()
{
    delete ui;
}

void AlbumsLibrary::SetPointers(playlist *PP, player *PlP, PlayerWindow *Plw)
{
    PlaylistPointer = PP;
    PlayerPointer = PlP;
    PlayerWindowPointer = Plw;
    lista = PlaylistPointer->getPlaylist();
    ListFolders();
    if(PlaylistPointer) connect(PlaylistPointer, SIGNAL(returnPlaylistName(QString)), this, SLOT(GetPlalylsistName(QString)));
}

void AlbumsLibrary::ListFolders() //zaladowanie elementow do biblioteki
{
    //QMediaPlayer TempPlayer;
    if(ui->stackedWidget->currentIndex() == 0) close();
    ClearButtonsTab();
    bool IsContinue=true;
//folders << "";
//QDirIterator directories("/home/lukas/Muzyka", QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot, QDirIterator::Subdirectories); //For PC
QDirIterator directories(FolderPath, QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot, QDirIterator::Subdirectories); //For RaspberryPi
//QString abc;

//int a=1;
if(!directories.hasNext())
{
    ui->EmptyLabel->setVisible(true);
    return;
}
    ui->EmptyLabel->setVisible(false);
   do{

 //while(a <= 19){ //tymczasowe
        if(directories.hasNext())
        {
            directories.next();
            folder.setPath(directories.filePath());
        }
        else
        {
            folder.setPath(OtherTracksPath);
            IsContinue = false;
        }

        QStringList TempFilesList = folder.entryList(QStringList() << "*.flac" <<"*.mp3",QDir::Files);
        if(TempFilesList.isEmpty()) continue;   //Jesli nie ma plikow muzycznych to pomijamy folder



        image_files = folder.entryList(QStringList() << "*.jpg" <<"*.png",QDir::Files);

        if(!image_files.isEmpty())
            setPixmapImg(folder.path(),image_files.first());
        else
        {
            //TempPlayer.setMedia(QUrl::fromLocalFile(folder.path()+"/"+TempFilesList.first()));

            //QImage TempImage = TempPlayer.metaData("CoverArtImage").value<QImage>();

            //if(TempImage.isNull())
                setPixmapImg("/home/pi","albumart.jpg");
//            else
//                setPixmapImg(TempImage);
        }



//        if(image_files.isEmpty())
//            setPixmapImg("/home/pi","albumart.jpg");

       //nazwy = directories.fileName();
       AddButton(folder.path());
     //  directories.next();

//a++;
image_files.clear();
}while(IsContinue);
LoadAlbumsLibrary(0);
if(!button_tab.isEmpty() && button_tab.length() > MaxColumns*MaxRows)
{
    ui->NextPage->setVisible(true);
    ui->PrevPage->setVisible(true);
}
else
{
    ui->NextPage->setVisible(false);
    ui->PrevPage->setVisible(false);
}
}

void AlbumsLibrary::LoadAlbumsLibrary(int Page)
{
    int pos = button_tab.length()-1;
    //int StartPoint = 0;
    int StartPoint = MaxRows*MaxColumns*Page;
    if(StartPoint > pos) return;
    int EndPoint = (MaxRows*MaxColumns*(Page+1))-1;
    if(EndPoint > pos) EndPoint = pos;
    x=0;
    y=0;
    for(int i=0; i <= pos;i++)
    {
        if(i >= StartPoint && i <=EndPoint )
        {
            button_tab[i]->setVisible(true);
            ui->AlbumsGrid->addWidget(button_tab[i],x,y);
            x++;
            if(x == MaxRows)
            {
                x=0;
                y++;
            }
        }
        else button_tab[i]->setVisible(false);
    }
}

void AlbumsLibrary::ClearButtonsTab()
{
    if(button_tab.isEmpty()) return;
    for(int i=0; i < button_tab.length(); i++)
    {
        delete button_tab[i];
    }
    button_tab.clear();
}

void AlbumsLibrary::OpenInSelectAlarmMode(int CurrMode)
{
    if(CurrMode == SelectedTrackMod)
    {
        SelectAlarmMode = true;
        ui->SelectAlarmLabel->setVisible(true);
        ui->PlayerButton->setVisible(false);
        show();
    }
    else if(CurrMode == Radio)
    {
        QStringList TempRadioList = PlaylistPointer->LoadSavedRadioStations();
        connect(&Popup, SIGNAL(ReturnValue(QString)), this, SLOT(ReceiveFullRadioEntry(QString)));
        disconnect(&Popup, SIGNAL(ReturnValue(QString)), this, SLOT(AddToSelectedPlaylist(QString)));
        Popup.SetAsSelectOptionWindow("Wybierz stacje radiowa:", TempRadioList, true);
        Popup.show();
    }


}

void AlbumsLibrary::AddButton(QString filepath) //funkcja do dynamicznego dodawania przyciskow do biblioteki
{



    button_tab.push_back(new QPushButton);

    int pos = button_tab.length()-1;

//    ui->AlbumsGrid->addWidget(button_tab[pos],x,y);
    button_tab[pos]->setMaximumHeight(50);
    button_tab[pos]->setMaximumWidth(50);
    button_tab[pos]->setMinimumHeight(50);
    button_tab[pos]->setMinimumWidth(50);
    button_tab[pos]->setStyleSheet("background-color: transparent;border: none; ");
    button_tab[pos]->setIconSize(QSize(50,50));

   connect(button_tab[pos],&QPushButton::clicked,[=](){SetAlbumPage(filepath);}); //przypisywanie funkcji do przyciskow

  if(!image.isNull())  button_tab[pos]->setIcon(image);


//    x++;
    //pos++;
//    if(x == MaxRows)
//    {
//        x=0;
//        y++;
//    }

}


QPixmap AlbumsLibrary::setPixmapImg(QString sciezka, QString plik)
{
    image.load(sciezka + "/" + plik);

    //image = image.scaledToWidth(ui->label_image->width(), Qt::SmoothTransformation);
    //image = image.scaledToHeight(ui->label_image->height(), Qt::SmoothTransformation);
    return image;
}

void AlbumsLibrary::setPixmapImg(QImage Image)
{
    image.convertFromImage(Image);
}

void AlbumsLibrary::PreparePopupDialog(int TrackNumber)
{
    PlList.clear();
    PlaylistPointer->list_playlists();
    SelectedTrack = TrackNumber;
    disconnect(&Popup, SIGNAL(ReturnValue(QString)), this, SIGNAL(ReceiveFullRadioEntry(QString)));
    connect(&Popup, SIGNAL(ReturnValue(QString)), this, SLOT(AddToSelectedPlaylist(QString)));
    Popup.SetAsSelectOptionWindow("Wybierz playliste: ", PlList, true);
    Popup.show();
}

void AlbumsLibrary::AddToSelectedPlaylist(QString PlaylistName)
{
    if(PlaylistPointer->AddSpecifiedTrackToPlaylist(PlaylistName, (folder.path()+"/" +files[SelectedTrack])))
    {
        QFileInfo FInfo;
        FInfo.setFile(files[SelectedTrack]);
        PlayerWindowPointer->AddToTrackList(FInfo.completeBaseName());
    }
    //qDebug () << "Path: " << folder.path()+"/" +files[SelectedTrack];
}

void AlbumsLibrary::AddToFavourites(int TrackNumber)
{
    SelectedTrack = TrackNumber;
    AddToSelectedPlaylist("playlist");
    CustomAlbumRow *TempPoint;
    TempPoint = qobject_cast<CustomAlbumRow *> (ui->Tracklist->itemWidget(ui->Tracklist->item(TrackNumber)));
    disconnect(TempPoint->FavouriteButton, 0, 0, 0); //przypisywanie funkcji do przyciskow

}

void AlbumsLibrary::ReceiveFullRadioEntry(QString Index)
{
    emit SendTrackPath(PlaylistPointer->GetRadioFullEntry(Index.toInt()));
}

void AlbumsLibrary::SetAlbumPage(QString sciezka) //wczytanie wybranego folderu do storny albumu
{
//    QMediaPlayer TempPlayer;
    int NumberOfItems=0;
    content.clear();
   folder.setPath(sciezka);
   ui->TitleLabel->setText(folder.dirName());
    files = folder.entryList(QStringList() << "*.flac" <<"*.mp3",QDir::Files);
    image_files = folder.entryList(QStringList() << "*.jpg" <<"*.png",QDir::Files); //do okladki

    ui->Tracklist->clear();

    for(const QString& f:files) // Do sprawdzenia
    {
        content.push_back(QUrl::fromLocalFile(folder.path()+"/" + f));
     fi.setFile(f);
     CustomAlbumRow *TempPoint = new CustomAlbumRow(this, AlbumMode);
     QListWidgetItem *TempPointItem = new QListWidgetItem;
    // connect(TempPoint->PlaylistButton, SIGNAL(clicked()), this, SLOT(PreparePopupDialog()));
     connect(TempPoint->PlaylistButton,&QPushButton::clicked,[=](){PreparePopupDialog(NumberOfItems);}); //przypisywanie funkcji do przyciskow
     connect(TempPoint->FavouriteButton,&QPushButton::clicked,[=](){AddToFavourites(NumberOfItems);}); //przypisywanie funkcji do przyciskow
     TempPointItem->setSizeHint(QSize(TempPointItem->sizeHint().height(), 40));
     TempPoint->SetLabel(fi.completeBaseName());
        ui->Tracklist->addItem(TempPointItem);
        ui->Tracklist->setItemWidget(TempPointItem, TempPoint);
     NumberOfItems++;

    }

 //   ui->Tracklist->setCurrentRow(lista->currentIndex() != -1? lista->currentIndex():0);   //do zrobienia


  if(!image_files.empty())  ui->CoverLabel->setPixmap(setPixmapImg(folder.path(),image_files.first()));
  else
  {
      //QImage TempImage = TempPlayer.metaData("CoverArtImage").value<QImage>();

      //if(TempImage.isNull())
          ui->CoverLabel->setPixmap(setPixmapImg("/home/pi","albumart.jpg"));
     // else ui->CoverLabel->setPixmap(QPixmap::fromImage(TempImage));
  }
//if(image_files.isEmpty()) ui->CoverLabel->setPixmap(setPixmapImg("/home/pi","albumart.jpg"));

IsAlbumPageOpened=true;
ui->stackedWidget->setCurrentIndex(1);

if(ui->NextPage->isVisible())
{
    ui->NextPage->setVisible(false);
    ui->PrevPage->setVisible(false);
}

}

void AlbumsLibrary::GetPlalylsistName(QString Text)
{
    PlList.push_back(Text);
}

void AlbumsLibrary::on_BackButton_clicked()
{
    if(IsAlbumPageOpened)
    {
        ui->stackedWidget->setCurrentIndex(0);
        IsAlbumPageOpened=false;
        if(button_tab.length() > MaxColumns*MaxRows)
        {
            ui->NextPage->setVisible(true);
            ui->PrevPage->setVisible(true);
        }
    }
    else
    {
        close();
        if(SelectAlarmMode)
        {
            SelectAlarmMode = false;
            ui->SelectAlarmLabel->setVisible(false);
            ui->PlayerButton->setVisible(true);
        }
    }
}

void AlbumsLibrary::on_NextPage_clicked()
{
    if(CurPage < button_tab.length()/(MaxColumns*MaxRows))
    {
        CurPage++;
        LoadAlbumsLibrary(CurPage);
    }
}

void AlbumsLibrary::on_PrevPage_clicked()
{
    if(CurPage > 0)
    {
        CurPage--;
        LoadAlbumsLibrary(CurPage);
    }
}

void AlbumsLibrary::on_Tracklist_clicked(const QModelIndex &index)
{
    if(!SelectAlarmMode)
    {
    PlaylistPointer->clear();
        PlaylistPointer->setPlaylist(content);

       //if(!image_files.empty()) ui->label_image->setPixmap(set_pixmap_img(folder.path(),image_files.first()));
       //if(image_files.isEmpty()) ui->label_image->setPixmap(set_pixmap_img("/home/lukas/Dokumenty","albumart.jpg"));

        PlaylistPointer->setIndex(ui->Tracklist->currentRow());
       // fprintf(stderr, "Click index: %i\n", ui->Tracklist->currentRow());
        //ui->listWidget->clear();
        PlayerWindowPointer->SetTrackList(files);
     //   if(!image_files.empty()) PlayerWindowPointer->SetCoverImage(QString(folder.path()+"/"+image_files.first()));
      //  else PlayerWindowPointer->SetCoverImage("/home/pi/albumart.jpg");
        PlayerPointer->odtworz(0);
    }
    else
    {
        QString ReturnVal = folder.path()+"/"+files[ui->Tracklist->currentRow()];
        SelectAlarmMode = false;
        ui->SelectAlarmLabel->setVisible(false);
        ui->PlayerButton->setVisible(true);
        ui->stackedWidget->setCurrentIndex(0);
        IsAlbumPageOpened=false;
        emit SendTrackPath(ReturnVal);
        //qDebug () <<"Path: " << ReturnVal;
        close();
        ui->NextPage->setVisible(true);
        ui->PrevPage->setVisible(true);
    }
}

void AlbumsLibrary::on_PlayerButton_clicked()
{
    PlayerWindowPointer->show();
}
