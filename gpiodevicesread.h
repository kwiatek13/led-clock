#ifndef GPIODEVICESREAD_H
#define GPIODEVICESREAD_H

#include <QObject>
#include <QThread>
#include <QDebug>
#include <chrono>
#include <ctime>
#include <QVector>
#include "wiringPi.h"
#include "RCSwitch.h"

static const int ButtonPin = 4;
static const int DHTPin = 3;
static const int ReceiverPin = 5;



struct DeviceData
{
    bool IsSnoozeButtonPushed = false;
    unsigned long RCCode = 0;
    int TypedCombination = 0;
};

class GPIODevicesRead : public QObject
{
    Q_OBJECT
public:
    explicit GPIODevicesRead(DeviceData *Pointer);
    DeviceData *SharedPointer;
    void SnoozeButtonRead();
    void read_dht_data();
    void ReadReceiverData();
        RCSwitch RadioSwitch;

private:
        void CheckCombination();
    int DHTData[5] = { 0, 0, 0, 0, 0 };

        int pulseLength = 24;
        int ResetCounter = 0;
        unsigned long LastRCState=0;
        QVector <unsigned long> Combination;
        std::chrono::time_point<std::chrono::system_clock> CombinationTime;

        unsigned long ButtonA, ButtonB, ButtonC, ButtonD;

signals:
        void RCButtonPressed(unsigned long Code);
        void CombinationTyped(int CurrentCombination);

public slots:
    void ReadProcess();
};

#endif // GPIODEVICESREAD_H
