#include "ledcontrol.h"

LedControl::LedControl(QObject *parent) : QObject(parent)
{
    LedController = new LedEngine(&LightMode, &LightTime, &ColorSetter, &SharedMutex);
    connect(LedController, SIGNAL(Finish()), &EngineThread, SLOT(quit()));
    connect(&EngineThread, SIGNAL(started()), LedController, SLOT(StartProcess()));
    LedController->moveToThread(&EngineThread);
}

void LedControl::LaunchSunrise(int RunningTime)
{
    SharedMutex.lock();
    LightTime = RunningTime;
    LightMode = SunriseMode;
    SharedMutex.unlock();
    if(!EngineThread.isRunning()) EngineThread.start();
}

void LedControl::OffLed()
{
    SharedMutex.lock();
    LightMode = SmoothOffMode;
    SharedMutex.unlock();
    if(!EngineThread.isRunning()) EngineThread.start();
}

void LedControl::LaunchLampMode(QColor Color)
{
    SharedMutex.lock();
    ColorSetter = Color;
    LightMode = LampMode;
    SharedMutex.unlock();
    if(!EngineThread.isRunning()) EngineThread.start();

}
//S - 95 - 15
//V - 30 - 100

LedEngine::LedEngine(int *Mode, int *LightTime, QColor *ColorSet, QMutex *MutexP)
{
    SharedColorSetter = ColorSet;
    SharedLightTime = LightTime;
    SharedMode = Mode;
    MutexPtr = MutexP;
    if(wiringPiSetup() < 0) { //when initialize wiringPi failed, print message to screen
        printf("setup wiringPi failed !\n");
        //return -1;
    }

    ledInit();
}

int LedEngine::TestLed()
{
    //int i=0;

    //bool IsIncrement = true;

    DelayTime = (*SharedLightTime*1000)/255;

    while(1) {
        //int S = 95;
        int LocalS = 255;
        int LocalV = 150;
        //int DirectS, DirectV;
        //        for(i = 0; i < sizeof(colors)/sizeof(int); i++) {
        //            ledColorSet(colors[i]);
        //            delay(1000);
        //ColorSetter.setHsv(189, 84, i);
        while(S > 0)
        {
            //DirectS = S*2,5
            ColorSetter.setHsv(20, LocalS, LocalV);
            qDebug () <<"Red: " <<ColorSetter.red() <<"Green: " <<ColorSetter.green() <<"Blue: " <<ColorSetter.blue() <<"\n";

            ledColorSet(ColorSetter.red(), ColorSetter.green(), ColorSetter.blue());


            //            if(IsIncrement) i += 1;
            //            else i -= 1;

            //            if(i >= 100) IsIncrement = false;
            //            else if(i <= 0) IsIncrement = true;
            if(S >= 0) LocalS--;
            if(V < 255 && S%2 == 0) LocalV++;
            QThread::msleep(DelayTime);
        }
        //}
    }
    return 0;
}

int LedEngine::map(int x, int in_min, int in_max, int out_min, int out_max)
{
    return (x -in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void LedEngine::ledInit()
{
    softPwmCreate(LedPinRed,  0, 100);  //create a soft pwm, original duty cycle is 0Hz, range is 0~100
    softPwmCreate(LedPinGreen,0, 100);
    softPwmCreate(LedPinBlue, 0, 100);
}

void LedEngine::ledColorSet(int color)
{
    int r_val, g_val, b_val;

    r_val = (color & 0xFF0000) >> 16;  //get red value
    g_val = (color & 0x00FF00) >> 8;   //get green value
    b_val = (color & 0x0000FF) >> 0;   //get blue value

    r_val = map(r_val, 0, 255, 0, 100);    //change a num(0~255) to 0~100
    g_val = map(g_val, 0, 255, 0, 100);
    b_val = map(b_val, 0, 255, 0, 100);

    softPwmWrite(LedPinRed,   r_val);  //change duty cycle
    softPwmWrite(LedPinGreen, g_val);
    softPwmWrite(LedPinBlue,  b_val);
}

void LedEngine::ledColorSet(int r, int g, int b)
{
    r = map(r, 0, 255, 0, 100);    //change a num(0~255) to 0~100
    g = map(g, 0, 255, 0, 100);
    b = map(b, 0, 255, 0, 100);

    softPwmWrite(LedPinRed,   r);  //change duty cycle
    softPwmWrite(LedPinGreen, g);
    softPwmWrite(LedPinBlue,  b);
}

void LedEngine::SimulateSunrise()
{


  //  while(1) {
        //int S = 255;
       // int V = 150;
             S--;

            //DirectS = S*2,5
            ColorSetter.setHsv(H, S, V);
            //qDebug () <<"Red: " <<ColorSetter.red() <<"Green: " <<ColorSetter.green() <<"Blue: " <<ColorSetter.blue() <<"\n";

            ledColorSet(ColorSetter.red(), ColorSetter.green(), ColorSetter.blue());


            //            if(IsIncrement) i += 1;
            //            else i -= 1;

            //            if(i >= 100) IsIncrement = false;
            //            else if(i <= 0) IsIncrement = true;

            if(V < 255 && S%2 == 0) V++;

            MutexPtr->lock();
            int TempMode = *SharedMode;
            MutexPtr->unlock();

            if(S > 0 && LastMode == TempMode) QTimer::singleShot(DelayTime, this, SLOT(SimulateSunrise()));
            else if(LastMode != TempMode) StartProcess();
            else if(S == 0)
            {
                MutexPtr->lock();
                emit Finish();
                MutexPtr->unlock();
            }


            //QThread::msleep(DelayTime);

        //}
            //  }
}

void LedEngine::SmoothOff()
{
    if(V >= 0)
    {
        ColorSetter.setHsv(H,S,V);
        ledColorSet(ColorSetter.red(), ColorSetter.green(), ColorSetter.blue());
    }

    V--;

    MutexPtr->lock();
    int TempMode = *SharedMode;
    MutexPtr->unlock();

    if(V >= 0 && LastMode == TempMode) QTimer::singleShot(25, this, SLOT(SmoothOff()));
    else if(LastMode != TempMode) StartProcess();
    else if(V < 0)
    {
        MutexPtr->lock();
        emit Finish();
        MutexPtr->unlock();
    }



}

void LedEngine::EnableLamp()
{
    int CheckVal = 0;

    if(H < SharedColorSetter->hue())
    {
        H++;
        CheckVal++;
    }
    else if(H > SharedColorSetter->hue())
    {
        H--;
        CheckVal++;
    }

    if(S < SharedColorSetter->saturation())
    {
        S++;
        CheckVal++;
    }
    else if(S > SharedColorSetter->saturation())
    {
        S--;
        CheckVal++;
    }

    if(V < SharedColorSetter->value())
    {
        V++;
        CheckVal++;
    }
    else if(V > SharedColorSetter->value())
    {
        V--;
        CheckVal++;
    }

    //qDebug () <<"H: " <<H <<" S: " <<S <<" V: " <<V <<"\n";
    if(H >= 0 && S >= 0 && V >= 0)
    {
        ColorSetter.setHsv(H,S,V);

        ledColorSet(ColorSetter.red(), ColorSetter.green(), ColorSetter.blue());
    }

    MutexPtr->lock();
    int TempMode = *SharedMode;
    MutexPtr->unlock();

    if(CheckVal > 0 && LastMode == TempMode) QTimer::singleShot(30, this, SLOT(EnableLamp()));
    else if(LastMode != TempMode) StartProcess();
    else if(CheckVal == 0)
    {
        MutexPtr->lock();
        emit Finish();
        MutexPtr->unlock();
    }


}

void LedEngine::StartProcess()
{
    MutexPtr->lock();
    LastMode = *SharedMode;
    MutexPtr->unlock();
    switch(LastMode)
    {
    case SunriseMode:
        H = 20;
        S = 256;
        V = 150;
        MutexPtr->lock();
        DelayTime = (*SharedLightTime*1000)/255;
        MutexPtr->unlock();
        qDebug () <<"Sunrise mode! \n";
        SimulateSunrise();
        break;

    case SmoothOffMode:
        SmoothOff();
        break;

    case LampMode:
        EnableLamp();
        break;

    }
    MutexPtr->lock();
    if(LastMode != *SharedMode)
    {
        MutexPtr->unlock();
        StartProcess();
    }
    else
        MutexPtr->unlock();
     //else emit Finish();
}
