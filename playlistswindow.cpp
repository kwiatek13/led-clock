#include "playlistswindow.h"
#include "ui_playlistswindow.h"

PlaylistsWindow::PlaylistsWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PlaylistsWindow)
{
    ui->setupUi(this);
    PopDialog.SetAsEnterWindow("Podaj nazwe playlisty: ");
    PopNewRadio.SetAsEnterWindowWith2Args("Podaj nazwe wlasna stacji:", "Podaj adres URL stacji:");
    connect(&PopDialog, SIGNAL(ReturnValue(QString)), this, SLOT(CreatePlaylist(QString)));
    connect(&PopDelete, SIGNAL(ReturnValue(QString)), this, SLOT(GetAgree(QString)));
    connect(&PopNewRadio, SIGNAL(ReturnValue(QString)), this, SLOT(CreateNewRadioEntry(QString)));

}

PlaylistsWindow::~PlaylistsWindow()
{
    delete ui;
}

void PlaylistsWindow::SetPointers(playlist *PP, player *PlP, PlayerWindow *Plw)
{
    PlaylistPointer = PP;
    PlayerPointer = PlP;
    PlayerWindowPointer = Plw;
 //   connect(PlaylistPointer, SIGNAL(returnPlaylistName(QString)), this, SLOT(AddEntryToPlalistsList(QString)));
    connect(PlaylistPointer, SIGNAL(returnPath(QString)), this, SLOT(AddEntryToSelectedPlaylistList(QString)));
    connect(PlaylistPointer, SIGNAL(returnPathMain(QString)), this, SLOT(AddTracksFromPlaylist(QString)));
    connect(PlaylistPointer, SIGNAL(DeleteFromList(int)), this, SLOT(DeleteCurrentPosistion(int)));
}

void PlaylistsWindow::RefreshPlaylistsList()
{
    ui->PlaylistsList->clear();
    ui->SelectedPlaylistList->clear();
    PlaylistPointer->readPlaylist("playlist");
    PlaylistPointer->list_playlists();
  //      ui->PlaylistsList->clear();
}

void PlaylistsWindow::LaunchSignals()
{
    connect(PlaylistPointer, SIGNAL(returnPlaylistName(QString)), this, SLOT(AddEntryToPlalistsList(QString)));
    if(CurrentMode == RadioMode)
    {
        CurrentMode = LocalMode;
        RefreshPlaylistsList();
    }
}

void PlaylistsWindow::DisconnectSignals()
{
    disconnect(PlaylistPointer, SIGNAL(returnPlaylistName(QString)), this, SLOT(AddEntryToPlalistsList(QString)));
}

void PlaylistsWindow::ListSavedRadioStations()
{

    CurrentMode = RadioMode;
    ui->PlaylistsList->clear();
    QStringList TempList = PlaylistPointer->LoadSavedRadioStations();
    NumberOfIndex=0;
    if(TempList.isEmpty()) return;

    for(int a=0; a < TempList.size(); a++)
    {
        AddEntryToRadioList(TempList[a]);
    }

}

void PlaylistsWindow::AddEntryToPlalistsList(QString playlist_path)
{
    QListWidgetItem *TempPointItem = new QListWidgetItem;
    TempPointItem->setSizeHint(QSize(TempPointItem->sizeHint().height(), 40));
    ui->PlaylistsList->addItem(TempPointItem);
    TempPointItem->setText(playlist_path);

    if(playlist_path != "playlist")
    {
        CustomAlbumRow *TempPoint = new CustomAlbumRow(this, PlaylistMode);
        connect(TempPoint->FavouriteButton,&QPushButton::clicked,[=](){DeletePlaylist(playlist_path);}); //przypisywanie funkcji do przyciskow
        //TempPoint->SetLabel(playlist_path);

        ui->PlaylistsList->setItemWidget(TempPointItem, TempPoint);
    }
    else
    {
       // TempPointItem->setText(playlist_path);
    }
    //ui->PlaylistsList->addItem(playlist_path);


}

void PlaylistsWindow::on_PlaylistsList_clicked(const QModelIndex &index)
{
    if(CurrentMode == LocalMode)
    {
    ui->stackedWidget->setCurrentIndex(1);
    ui->AddButton->setVisible(false);
    ui->SelectedPlaylistList->clear();
    NumberOfIndex=0;
    PlaylistPointer->readPlaylist(PlaylistPointer->returnPlaylistPath(ui->PlaylistsList->currentRow()));
    }
    else if(CurrentMode == RadioMode)
    {
        PlaylistPointer->SetRadioStation(ui->PlaylistsList->currentRow());
        PlayerPointer->odtworz(0);
        PlayerWindowPointer->SetRadioMode(ui->PlaylistsList->currentItem()->text());
    }
}

void PlaylistsWindow::AddEntryToSelectedPlaylistList(QString track_path)
{
    QFileInfo fi;
    fi.setFile(track_path);
    int TempVal = NumberOfIndex;
    QListWidgetItem *TempPointItem = new QListWidgetItem;
    TempPointItem->setSizeHint(QSize(TempPointItem->sizeHint().height(), 40));
    TempPointItem->setText(fi.completeBaseName());
    ui->SelectedPlaylistList->addItem(TempPointItem);

    CustomAlbumRow *TempPoint = new CustomAlbumRow(this, PlaylistMode);
    connect(TempPoint->FavouriteButton,&QPushButton::clicked,[=](){DeleteTrack(TempVal);}); //przypisywanie funkcji do przyciskow
   // TempPoint->SetLabel(fi.completeBaseName());
    ui->SelectedPlaylistList->setItemWidget(TempPointItem, TempPoint);
    NumberOfIndex++;
    //ui->SelectedPlaylistList->addItem(fi.completeBaseName());
}

void PlaylistsWindow::AddEntryToRadioList(QString Title)
{
    QListWidgetItem *TempPointItem = new QListWidgetItem;
    TempPointItem->setSizeHint(QSize(TempPointItem->sizeHint().height(), 40));
    ui->PlaylistsList->addItem(TempPointItem);
    TempPointItem->setText(Title);

        CustomAlbumRow *TempPoint = new CustomAlbumRow(this, PlaylistMode);
        int TempNum = NumberOfIndex;
        connect(TempPoint->FavouriteButton,&QPushButton::clicked,[=](){DeleteRadioStation(TempNum);}); //przypisywanie funkcji do przyciskow
        //TempPoint->SetLabel(playlist_path);

        ui->PlaylistsList->setItemWidget(TempPointItem, TempPoint);
        NumberOfIndex++;
}

void PlaylistsWindow::OpenRadioMode()
{
    if(CurrentMode != RadioMode)
    {
       ListSavedRadioStations();
    }
    show();
}

void PlaylistsWindow::AddTracksFromPlaylist(QString track_path)
{
    TracksList << track_path;
}

void PlaylistsWindow::on_SelectedPlaylistList_clicked(const QModelIndex &index)
{
    PlaylistPointer->clear();
    TracksList.clear();
    PlaylistPointer->add_current();
    PlaylistPointer->setIndex(ui->SelectedPlaylistList->currentRow());
    //ui->label_image->setPixmap(set_pixmap_img("/home/lukas/Dokumenty","albumart.jpg"));
    PlayerWindowPointer->SetTrackList(TracksList);
    PlayerPointer->odtworz(0);
}

void PlaylistsWindow::on_BackButton_clicked()
{
    if(ui->stackedWidget->currentIndex() == 0)
    {
        if(CurrentMode == LocalMode) DisconnectSignals();
        close();
    }
    else
    {
        ui->stackedWidget->setCurrentIndex(0);
        ui->AddButton->setVisible(true);
    }
}

void PlaylistsWindow::on_PlayerButton_clicked()
{
    PlayerWindowPointer->show();
}

void PlaylistsWindow::on_AddButton_clicked()
{
    if(CurrentMode == LocalMode) PopDialog.show();
    else if(CurrentMode == RadioMode) PopNewRadio.show();
}

void PlaylistsWindow::CreatePlaylist(QString Name)
{
    PlaylistPointer->create_playlist(Name);
    RefreshPlaylistsList();
}

void PlaylistsWindow::DeletePlaylist(QString Name)
{
    PopDelete.SetAsAcceptWindowWithReturn(QString("Czy na pewno chcesz usunac playliste: %1?").arg(Name), Name);
    DelMode = DeleteOnePlaylist;
    PopDelete.show();
}


void PlaylistsWindow::GetAgree(QString Val)
{
    switch(DelMode)
    {
    case DeleteOnePlaylist:{
        PlaylistPointer->readPlaylist(QString(Val+".pla"));
        PlaylistPointer->delete_current_playlist();
        RefreshPlaylistsList();
    }
        break;

    case DeleteSingleTrack:{
        QString TempPlName = ui->PlaylistsList->currentItem()->text();
        if(TempPlName != "playlist") TempPlName += ".pla";
        PlaylistPointer->DeleteEntryFromSpecifiedPlaylist(TempPlName, Val.toInt());
        ui->SelectedPlaylistList->clear();
        NumberOfIndex=0;
        PlaylistPointer->readPlaylist(PlaylistPointer->returnPlaylistPath(ui->PlaylistsList->currentRow()));
    }
        break;

    case DeleteRadioStationMode:{
        PlaylistPointer->DeleteSelectedRadio(Val.toInt());
        ListSavedRadioStations();
    }
        break;
    }

}

void PlaylistsWindow::DeleteTrack(int Index)
{
    PopDelete.SetAsAcceptWindowWithReturn(QString("Czy na pewno chcesz usunac utwor: %1?").arg(ui->SelectedPlaylistList->item(Index)->text()), QString::number(Index));
    DelMode = DeleteSingleTrack;
    PopDelete.show();
}

void PlaylistsWindow::DeleteCurrentPosistion(int Index)
{
    PlayerWindowPointer->DeleteFromList(Index);
}

void PlaylistsWindow::CreateNewRadioEntry(QString Entry)
{
    PlaylistPointer->AddNewRadioEntry(Entry);
    QString TempLine;
    for(int x=0; x < Entry.size(); x++)
    {
        if(Entry[x] == ";") break;
        TempLine += Entry[x];
    }

    AddEntryToRadioList(TempLine);

}

void PlaylistsWindow::DeleteRadioStation(int Index)
{
    PopDelete.SetAsAcceptWindowWithReturn(QString("Czy na pewno chcesz usunac stacja radiowa: %1?").arg(ui->PlaylistsList->item(Index)->text()), QString::number(Index));
    DelMode = DeleteRadioStationMode;
    PopDelete.show();
}
