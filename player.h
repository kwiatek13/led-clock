#ifndef PLAYER_H
#define PLAYER_H

//#include "dialog.h"
//#include "playlist.h"
#include <QDialog>
#include <QObject>
#include <QFileDialog>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <iostream>
#include <QSharedPointer>
#include <memory>

class playlist;

static const int MaxVolume=76;

class player: public QObject
{
    Q_OBJECT
int music_volume=100, shuffle_val=0;
std::shared_ptr<QMediaPlayer> music2=nullptr;
std::shared_ptr<QMediaPlaylist> m_list=nullptr;

public:
    player();

  //  playing();
    void odtworz(int tryb);
    void losuj();
    void seek(int pos);
    void NextTrack();
    void PreviousTrack();
    QMediaPlayer* getPlayer();
    void setPlaylist(std::shared_ptr<QMediaPlaylist> mpl);

signals:
    void PlaybackStateChanged(bool IsPlaying);



};

#endif // PLAYER_H
