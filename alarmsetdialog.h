#ifndef ALARMSETDIALOG_H
#define ALARMSETDIALOG_H

#include <QDialog>
#include <QDate>
#include <QDateTime>
#include <QFileInfo>
#include "alarmset.h"
#include "popupdialog.h"

enum Days : int
{
    Monday=0,
    Tuesday,
    Wendesday,
    Thursday,
    Friday,
    Saturday,
    Sunday
};

namespace Ui {
class AlarmSetDialog;
}

class AlarmSetDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AlarmSetDialog(QWidget *parent = 0);
    ~AlarmSetDialog();

public slots:
    void LoadAlarmEntry(AlarmSet *point);
    void GetAlrValue(QString Val);


private slots:
    void on_MoreHour_clicked();

    void on_LessHour_clicked();

    void on_MoreMinutes_clicked();

    void on_LessMinutes_clicked();


    void on_MoreMonths_clicked();

    void on_LessMonths_clicked();

    void on_MoreDays_clicked();

    void on_MoreYears_clicked();

    void on_LessDays_clicked();

    void on_LessYears_clicked();

    void on_BackButton_clicked();

    void on_HoursEdit_textChanged(const QString &arg1);

    void on_MinutesEdit_textChanged(const QString &arg1);

    void on_YearsEdit_textChanged(const QString &arg1);

    void on_MonthsEdit_textChanged(const QString &arg1);

    void on_DaysEdit_textChanged(const QString &arg1);


    void on_OnceCheckBox_stateChanged(int arg1);

    void SaveAlarm();

    void on_SignalCombo_currentIndexChanged(int index);

    void on_AlbumButton_clicked();

    void on_SunriseCheckBox_stateChanged(int arg1);

    void on_MoreSunrise_clicked();

    void on_LessSunrise_clicked();

signals:
    void RefreshStates();
    void SaveData();
    void OpenSetWindow(int Mode);

private:
    Ui::AlarmSetDialog *ui;
    AlarmSet* AlarmPointer;
    void SetDayToDate();
    QString PathForTrack;
    QString URLForRadio;
    void ShowSelectedTrackMode();
    void ShowSelectedRadioMode();
    PopupDialog PopWindow;
};

#endif // ALARMSETDIALOG_H
