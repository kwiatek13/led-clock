#include "alarmwindow.h"
#include "ui_alarmwindow.h"

AlarmWindow::AlarmWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AlarmWindow)
{
    ui->setupUi(this);
}

AlarmWindow::~AlarmWindow()
{
    delete ui;
}

void AlarmWindow::ShowWindow()
{
    show();
    AlarmStartTime = QTime::currentTime();
    IsOpenedWindow=true;
    RefreshTime();
}

void AlarmWindow::SetPointerToSharedData(DeviceData *SharedPointer)
{
    SharedDevicesData = SharedPointer;
}

void AlarmWindow::on_OffBtn_clicked()
{
    emit OffSignal();
    IsOpenedWindow=false;
    close();
}

void AlarmWindow::on_SnoozeBtn_clicked()
{
    emit SnoozeSignal();
    IsOpenedWindow=false;
    close();
}

void AlarmWindow::RefreshTime()
{
    //ui->TimeLabel->setText(QTime::currentTime().toString("hh:mm"));
    if(AlarmStartTime.secsTo(QTime::currentTime()) > AlarmTimeoutInSeconds || SharedDevicesData->IsSnoozeButtonPushed) on_SnoozeBtn_clicked();
    if(IsOpenedWindow) QTimer::singleShot(80, this, SLOT(RefreshTime()));
    //fprintf(stderr, "Refresh Time!\n");
}
