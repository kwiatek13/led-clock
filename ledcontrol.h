#ifndef LEDCONTROL_H
#define LEDCONTROL_H

#include <QObject>
#include <QDebug>
#include <QColor>
#include <QThread>
#include <QMutex>
#include <QTimer>
#include <stdio.h>
#include "wiringPi.h"
#include "softPwm.h"

#define LedPinRed    0
#define LedPinGreen  1
#define LedPinBlue   2

enum LedMode : int
{
    LampMode = 0,
    SunriseMode,
    SmoothOffMode
};

const int colors[] = {0xFF0000, 0x00FF00, 0x0000FF, 0xFFFF00, 0x00FFFF, 0xFF00FF, 0xFFFFFF, 0x9400D3};

class LedEngine : public QObject
{
    Q_OBJECT
public:
    explicit LedEngine(int *Mode, int *LightTime, QColor *ColorSet, QMutex *MutexP);

    int TestLed();
    //void SetSharedData();


private:
    QColor ColorSetter;
    int map(int x, int in_min, int in_max, int out_min, int out_max);
    void ledInit();
    void ledColorSet(int color);
    void ledColorSet(int r, int g, int b);
    int *SharedLightTime;
    int *SharedMode;
    QMutex *MutexPtr;
    int LastMode=0;
    QColor *SharedColorSetter;
    bool IsDone=false;

    int H, S, V = 0;
    int DelayTime = 0;


public slots:
    void StartProcess();

private slots:
    void SimulateSunrise();
    void SmoothOff();
    void EnableLamp();

signals:
    void Finish();

};

class LedControl : public QObject
{
    Q_OBJECT
public:
    explicit LedControl(QObject *parent = nullptr);
    void LaunchSunrise(int RunningTime);    //in seconds
    void OffLed();
    void LaunchLampMode(QColor Color);

private:
    QThread EngineThread;
    QMutex SharedMutex;
    LedEngine *LedController;
    QColor ColorSetter;
    int LightMode;
    int LightTime;



signals:

public slots:
};


#endif // LEDCONTROL_H
