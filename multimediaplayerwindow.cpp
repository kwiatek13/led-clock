#include "multimediaplayerwindow.h"
#include "ui_multimediaplayerwindow.h"

MultimediaPlayerWindow::MultimediaPlayerWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MultimediaPlayerWindow)
{
    ui->setupUi(this);
    //LocalPlaylist.setPlayer(LocalPlayer.getPlayer());
    LocalPlayer.setPlaylist(LocalPlaylist.getPlaylist());
    LocalPlayerWindow.SetPlayer(&LocalPlayer);
    LocalPlaylistWindow.SetPointers(&LocalPlaylist, &LocalPlayer, &LocalPlayerWindow);
    LocalAlbumsWindow.SetPointers(&LocalPlaylist, &LocalPlayer, &LocalPlayerWindow);
    LocalFilesManagerWindow.SetPointer(&LocalPlaylist);

    LocalPlaylistWindow.LaunchSignals();
    LocalPlaylistWindow.RefreshPlaylistsList();
    LocalPlaylistWindow.DisconnectSignals();

    MSgBox.setText("Odswiezanie biblioteki, prosze czekac...");

    connect(&LocalFilesManagerWindow, SIGNAL(RefreshLibrary()), this, SLOT(RefreshTime()));

    //connect(&LocalAlbumsWindow, SIGNAL(OpenPlayer()), this, SLOT(PlayerCall()));

}

MultimediaPlayerWindow::~MultimediaPlayerWindow()
{
    delete ui;
}

player *MultimediaPlayerWindow::GetPlayer()
{
    return &LocalPlayer;
}

playlist *MultimediaPlayerWindow::GetPlaylist()
{
    return &LocalPlaylist;
}

AlbumsLibrary *MultimediaPlayerWindow::GetAlbumsPtr()
{
    return &LocalAlbumsWindow;
}

void MultimediaPlayerWindow::on_MusicButton_clicked()
{
    //LocalPlaylistWindow.DisconnectSignals();
    LocalAlbumsWindow.show();
}

void MultimediaPlayerWindow::on_BackButton_clicked()
{
    close();
}

void MultimediaPlayerWindow::PlayerCall()
{
    LocalPlayerWindow.show();
}

void MultimediaPlayerWindow::on_PlaylistButton_clicked()
{
    LocalPlaylistWindow.LaunchSignals();
    LocalPlaylistWindow.show();
}

void MultimediaPlayerWindow::on_TestButton_clicked()
{
    //LocalPlaylist.fun_start();
  //  LocalPlayer.odtworz(0);

//    LocalFilesManagerWindow.ListUSBDriveFiles("/home/lk/");
    LocalFilesManagerWindow.show();
}

void MultimediaPlayerWindow::RefreshTime()
{
    //MSgBox.show();
    LocalAlbumsWindow.ListFolders();
   // MSgBox.close();
}

void MultimediaPlayerWindow::on_InternetRadioButton_clicked()
{
    //LocalPlayer.getPlayer()
    //LocalPlaylist.clear();
  //  LocalPlaylist.getPlaylist()->addMedia(QUrl("http://85.219.133.18/radio.php?id=-1&url=http://www.eskago.pl/radio/eska-warszawa"));
   // LocalPlayer.odtworz(0);
    LocalPlaylistWindow.OpenRadioMode();
}
