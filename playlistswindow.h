#ifndef PLAYLISTSWINDOW_H
#define PLAYLISTSWINDOW_H

#include <QDialog>
#include "playlist.h"
#include "player.h"
#include "playerwindow.h"
#include "popupdialog.h"
#include "customalbumrow.h"
#include <QFile>

static const int DeleteOnePlaylist=0;
static const int DeleteSingleTrack=1;
static const int DeleteRadioStationMode=2;

enum Modes
{
    LocalMode=0,
    RadioMode
};

namespace Ui {
class PlaylistsWindow;
}

class PlaylistsWindow : public QDialog
{
    Q_OBJECT

public:
    explicit PlaylistsWindow(QWidget *parent = 0);
    ~PlaylistsWindow();
    void SetPointers(playlist *PP, player *PlP, PlayerWindow *Plw);
    void RefreshPlaylistsList();
    void LaunchSignals();
    void DisconnectSignals();
    void ListSavedRadioStations();
    void AddEntryToRadioList(QString Title);
    void OpenRadioMode();

private:
    Ui::PlaylistsWindow *ui;
    playlist *PlaylistPointer;
    player *PlayerPointer;
    PlayerWindow *PlayerWindowPointer;
    QStringList TracksList;
    PopupDialog PopDialog;
    PopupDialog PopNewRadio;
    PopupDialog PopDelete;
    bool IsAccpeted=false;
    int DelMode=0;
    int NumberOfIndex=0;
    QFile RadioStationsFile;
    QVector <QString> RadioStationsNames;
    QVector <QString> RadioStationsUrls;
    int CurrentMode=LocalMode;

private slots:
    void AddEntryToPlalistsList(QString playlist_path);
    void AddEntryToSelectedPlaylistList(QString track_path);
    void AddTracksFromPlaylist(QString track_path);
    void on_PlaylistsList_clicked(const QModelIndex &index);
    void on_SelectedPlaylistList_clicked(const QModelIndex &index);
    void on_BackButton_clicked();
    void on_PlayerButton_clicked();
    void on_AddButton_clicked();
    void CreatePlaylist(QString Name);
    void DeletePlaylist(QString Name);
    void GetAgree(QString Val);
    void DeleteTrack(int Index);
    void DeleteCurrentPosistion(int Index);
    void CreateNewRadioEntry(QString Entry);
    void DeleteRadioStation(int Index);
};

#endif // PLAYLISTSWINDOW_H
