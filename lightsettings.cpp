#include "lightsettings.h"
#include "ui_lightsettings.h"

LightSettings::LightSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LightSettings)
{
    ui->setupUi(this);

    CurrentPalette = ui->ColorPreview->palette();
    ui->ColorPreview->setAutoFillBackground(true);
    SettingsFile.setFileName("/home/pi/LampSettings");
    CurrentColor.setHsv(255,255,255);
    LoadSettings();
}

LightSettings::~LightSettings()
{
    delete ui;
}

void LightSettings::SaveSettings()
{
    if(SettingsFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        QTextStream TextStream;
        TextStream.setDevice(&SettingsFile);
        TextStream << QString::number(CurrentColor.hue()) <<"\n";
        TextStream << QString::number(CurrentColor.saturation()) <<"\n";
        TextStream << QString::number(CurrentColor.value()) <<"\n";
        TextStream.flush();
        SettingsFile.close();
        IsSavedChanges = true;
    }
}

void LightSettings::LoadSettings()
{
    if(SettingsFile.open(QIODevice::ReadOnly))
    {
        HVal = SettingsFile.readLine().toInt();
        SVal = SettingsFile.readLine().toInt();
        VVal = SettingsFile.readLine().toInt();
        SettingsFile.close();
    }

    SetPreviewColor();
    ui->HueEdit->setText(QString::number(HVal));
    ui->SaturationEdit->setText(QString::number(SVal));
    ui->ValueEdit->setText(QString::number(VVal));

}

void LightSettings::SwitchLamp()
{
    if(LedPointer)
    {
        if(!IsEnabledLamp)
        {
            LedPointer->LaunchLampMode(CurrentColor);
            IsEnabledLamp = true;
        }
        else
        {
            LedPointer->OffLed();
            IsEnabledLamp = false;
        }
    }
}

void LightSettings::SetPointer(LedControl *Pointer)
{
    LedPointer = Pointer;
}

void LightSettings::on_BackButton_clicked()
{
   // if(LedPointer) LedPointer->OffLed();
    if(!IsSavedChanges) LoadSettings();
    close();
}

void LightSettings::on_MinusHue_clicked()
{
    if(HVal > 0)
    {
        HVal -=5;
        ui->HueEdit->setText(QString::number(HVal));
    }
    SetPreviewColor();
}

void LightSettings::on_PlusHue_clicked()
{
    if(HVal < 255)
    {
        HVal +=5;
        ui->HueEdit->setText(QString::number(HVal));
    }
    SetPreviewColor();
}

void LightSettings::on_MinusSaturation_clicked()
{
    if(SVal > 0)
    {
        SVal -=5;
        ui->SaturationEdit->setText(QString::number(SVal));
    }
    SetPreviewColor();
}

void LightSettings::on_PlusSaturation_clicked()
{
    if(SVal < 255)
    {
        SVal +=5;
        ui->SaturationEdit->setText(QString::number(SVal));
    }
    SetPreviewColor();
}

void LightSettings::on_MinusValue_clicked()
{
    if(VVal > 0)
    {
        VVal -=5;
        ui->ValueEdit->setText(QString::number(VVal));
    }
    SetPreviewColor();
}

void LightSettings::on_PlusValue_clicked()
{
    if(VVal < 255)
    {
        VVal +=5;
        ui->ValueEdit->setText(QString::number(VVal));
    }
    SetPreviewColor();
}

void LightSettings::SetPreviewColor()
{
    if(IsSavedChanges) IsSavedChanges = false;
    CurrentColor.setHsv(HVal, SVal, VVal);
    CurrentPalette.setColor(ui->ColorPreview->backgroundRole(), CurrentColor);
    ui->ColorPreview->setPalette(CurrentPalette);

}

void LightSettings::on_ApplyButton_clicked()
{
    if(LedPointer) LedPointer->LaunchLampMode(CurrentColor);
    SaveSettings();
}
