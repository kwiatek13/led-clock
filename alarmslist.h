#ifndef ALARMSLIST_H
#define ALARMSLIST_H

#include <QDialog>
#include <QPushButton>
#include <QFile>
#include <QTextStream>
#include "alarmset.h"
#include "alarmsetdialog.h"
#include "albumslibrary.h"

const int max_columns=5;

namespace Ui {
class AlarmsList;
}

class AlarmsList : public QDialog
{
    Q_OBJECT

public:
    explicit AlarmsList(QWidget *parent = 0);
    ~AlarmsList();
    void AddButtons(int indx);
    QVector <AlarmSet*> *Alarms;
    void SetAlarmsTabPointer(QVector <AlarmSet*> *p);
    void LoadAlarmsList();
    void DeleteAlarm(int pos);
    void AddAlarm();
    void ButtonsHandler(int index);
    void LoadFromFile();
    AlarmSet *FindClosestAlarm();
    void SetAlarmDate();
    void ConnectToAlbumWindow(AlbumsLibrary *point);



private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();


public slots:
    void RefreshButtons();
    void SaveToFile();


private:
    Ui::AlarmsList *ui;
    QVector <QPushButton*> ButtonsTab;
    QVector <AlarmSet*> *AlarmsTabPointer;
    AlarmSetDialog AlarmSetter;
    int x=0;
    int y=0;
    bool DeleteMode=false;
    int counter=0;
    QFile AlarmsFile;

signals:
    void LaunchCloseAlarmSet();
};

#endif // ALARMSLIST_H
