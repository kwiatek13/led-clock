#ifndef ALARMWINDOW_H
#define ALARMWINDOW_H

#include <QDialog>
#include <QTimer>
#include <QTime>
#include "gpiodevicesread.h"

const int AlarmTimeoutInSeconds=180;

namespace Ui {
class AlarmWindow;
}

class AlarmWindow : public QDialog
{
    Q_OBJECT

public:
    explicit AlarmWindow(QWidget *parent = 0);
    ~AlarmWindow();
    void ShowWindow();
    void SetPointerToSharedData(DeviceData *SharedPointer);

private slots:
    void on_OffBtn_clicked();

    void on_SnoozeBtn_clicked();

    void RefreshTime();

private:
    Ui::AlarmWindow *ui;
    bool IsOpenedWindow=false;
    QTime AlarmStartTime;
    DeviceData *SharedDevicesData;



signals:
    void OffSignal();
    void SnoozeSignal();
};

#endif // ALARMWINDOW_H
