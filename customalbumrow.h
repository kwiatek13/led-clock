#ifndef CUSTOMALBUMROW_H
#define CUSTOMALBUMROW_H

#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QLabel>
#include <QDebug>
#include <QCheckBox>

static const int PlaylistMode=0;
static const int AlbumMode=1;
static const int FileManagerMode=2;

class CustomAlbumRow : public QWidget
{
    Q_OBJECT
public:
    explicit CustomAlbumRow(QWidget *parent = nullptr, int Mode=PlaylistMode);
    ~CustomAlbumRow();
    void SetLabel(QString Text);
    QLabel NameLabel;
    QPushButton *FavouriteButton=nullptr;
    QPushButton *PlaylistButton=nullptr;
    QCheckBox *CheckBoxPtr=nullptr;
    QHBoxLayout HBLayout;


signals:

public slots:
    void SetColorToFavButton();
};

#endif // CUSTOMALBUMROW_H
