#include "playlist.h"
#include "player.h"
#include <QMediaPlaylist>

playlist::playlist()
{
    //playlist_file.setFileName("/home/lukas/playlist");      //For PC
    playlist_file.setFileName("/home/pi/playlist");         //For RaspberryPi
    RadioStationsFile.setFileName("/home/pi/RadioList");

    playlist_file.open(QIODevice::ReadWrite);

outStream.setDevice(&playlist_file);

music_list = std::make_shared<QMediaPlaylist>();

}


std::shared_ptr<QMediaPlaylist> playlist::getPlaylist()
{
  //  QMediaPlaylist* music_list_get;
  //  music_list_get = new QMediaPlaylist(music_list_get);

    auto music_list_get = music_list;

    //return music_list_get;
    return music_list;
}

void playlist::setPlaylist(QList<QMediaContent> content)
{
    music_list->addMedia(content);
    tracklist = content;
    if(playlist_file.isOpen()) playlist_file.close();
    playlist_file.setFileName("/home/pi/NullPl");
}

void playlist::setPlayer(QMediaPlayer *pl)
{
 if(created == 0)  music_list = std::make_shared<QMediaPlaylist>();
    created=1;
}


void playlist::next()
{
   music_list->next();
}

void playlist::previous()
{
    music_list->previous();
}

void playlist::clear()
{
    music_list->clear();
}

void playlist::setIndex(int pos)
{
    music_list->setCurrentIndex(pos);
}


void playlist::readPlaylist(QString pl_name)
{
    playlist_content.clear();
    if(playlist_file.exists()) playlist_file.close();
    //playlist_file.setFileName("/home/lukas/"+pl_name);      //For PC
    playlist_file.setFileName("/home/pi/"+pl_name);      //For RaspberryPi

    playlist_file.open(QIODevice::ReadWrite);

    while(!playlist_file.atEnd())
    {
        list.clear();
        buffer = playlist_file.readLine();
        list = buffer.split("\n");
        playlist_content.push_back(QUrl::fromLocalFile(list.first()));
        emit returnPath(buffer);
    }

    current_list = pl_name;

}

void playlist::writePlaylist(int position)
{

    outStream << tracklist[position].canonicalUrl().path() << "\n";
    outStream.flush();
}

void playlist::deleteEntry(QString list_pos)
{
    QString Target = list_pos;
    Target.remove("file://");
    Target += "\n";
    QString TempString;
     playlist_file.seek(0);
    del_buffer.clear();
   // selected_index=0;
    while(!playlist_file.atEnd())
    {
        TempString = playlist_file.readLine();
        if(TempString.contains("#")) TempString.remove("#");
        //if(list_pos != selected_index) del_buffer.push_back(TempString);
      //  qDebug () << "Target: " << Target;
      //  qDebug () << "TempString: " << TempString;
        if(Target != TempString) del_buffer.push_back(TempString);
     //   if(list_pos == selected_index) playlist_file.readLine();

        //selected_index++;
    }

    playlist_file.resize(0);
    for(int z=0;z<del_buffer.size();z++)
    {
       outStream << del_buffer[z];
    }
    outStream.flush();



}

void playlist::DeleteEntryFromSpecifiedPlaylist(QString PlaylistName, int pos)
{
    QFile PlFile;
    QTextStream PlStream;
   // qDebug () << "Playlist name: " <<PlaylistName <<"/n";
    PlFile.setFileName("/home/pi/"+PlaylistName);
    PlFile.open(QIODevice::ReadWrite);
    PlFile.seek(0);
    PlStream.setDevice(&PlFile);
    del_buffer.clear();
    selected_index=0;
    while(!PlFile.atEnd())
    {


        if(pos != selected_index) del_buffer.push_back(PlFile.readLine());
        if(pos == selected_index) PlFile.readLine();

        selected_index++;
    }

    PlFile.resize(0);
    for(int z=0;z<del_buffer.size();z++)
    {
        PlStream << del_buffer[z];
    }
    PlStream.flush();
    PlFile.close();

}


void playlist::fun_start()
{
    QString TempString;
poczatek:
    playlist_file.close();
    music_list->clear();
    playlist_content.clear();
    //playlist_file.setFileName("/home/lukas/fun_playlist");      //For PC
    playlist_file.setFileName("/home/pi/fun_playlist");      //For RaspberryPi

    playlist_file.open(QIODevice::ReadWrite);



    if(playlist_file.size()>0)
    {
        qsrand(QTime::currentTime().msec());

        // random_index = 1;
        while(!playlist_file.atEnd())
        {
            list.clear();
            buffer = playlist_file.readLine();
            list = buffer.split("\n");
            if(!(buffer.contains("#"))) playlist_content.push_back(QUrl::fromLocalFile(list.first()));
        }
        random_index = qrand()%playlist_content.size();
        music_list->addMedia(playlist_content[random_index]);

        deleteEntry(playlist_content[random_index].canonicalUrl().toString());
    }

    if(playlist_content.size() <= 1)
    {
        playlist_file.close();
        //playlist_file.setFileName("/home/lukas/playlist");      //For PC
        playlist_file.setFileName("/home/pi/playlist");      //For RaspberryPi
        playlist_file.open(QIODevice::ReadWrite);
        playlist_file.seek(0);
        del_buffer.clear();
        while(!playlist_file.atEnd())
        {
            TempString = playlist_file.readLine();
            //qDebug () << playlist_content[0].canonicalUrl().toString();
           // qDebug () << TempString;
            if(playlist_content.size() == 1)
            {
                QString TempString2 = playlist_content[0].canonicalUrl().toString();
                TempString2.remove("file://");
                TempString2 += "\n";
             //   qDebug () <<"TempString2: " << TempString2;
                if(TempString == TempString2)
                {
                    TempString2 = TempString;
                    TempString = "#";
                    TempString += TempString2;
                }
            }
            del_buffer.push_back(TempString);
        }

        playlist_file.close();
        //playlist_file.setFileName("/home/lukas/fun_playlist");      //For PC
        playlist_file.setFileName("/home/pi/fun_playlist");      //For RaspberryPi
        playlist_file.open(QIODevice::ReadWrite);
        playlist_file.resize(0);
        for(int z=0;z<del_buffer.size();z++)
        {
            outStream << del_buffer[z];
        }
        outStream.flush();
        if(playlist_content.size() == 0) goto poczatek;



    }
}


void playlist::add_current()
{
    music_list->clear();
    playlist_content.clear();
playlist_file.seek(0);
    while(!playlist_file.atEnd())
    {
        list.clear();
        buffer = playlist_file.readLine();
        list = buffer.split("\n");
        playlist_content.push_back(QUrl::fromLocalFile(list.first()));
        emit returnPathMain(buffer);
    }
        music_list->addMedia(playlist_content);
}


void playlist::list_playlists(bool InsideMode)
{
    //playlist_folder.setPath("/home/lukas/");        //For PC
    playlist_folder.setPath("/home/pi/");        //For RaspberryPi

    playlist_list.clear();
    playlist_list.push_back("playlist");
   playlist_list += playlist_folder.entryList(QStringList() << "*.pla", QDir::Files);
  //  playlist_list.push_back("playlist");
   if(!InsideMode)
   {
       for(const QString& f:playlist_list)
       {

           info.setFile(f);
           emit returnPlaylistName(info.completeBaseName());


       }
   }
}


QString playlist::returnPlaylistPath(int index_in_table)
{
    return playlist_list[index_in_table];
}

void playlist::create_playlist(QString name)
{
    playlist_file.close();
   // playlist_file.setFileName("/home/lukas/"+name+".pla");    //For PC
    playlist_file.setFileName("/home/pi/"+name+".pla");         //For RaspberryPi
    playlist_file.open(QIODevice::ReadWrite);
}

void playlist::delete_current_playlist()
{
    playlist_file.remove();
}

bool playlist::AddSpecifiedTrackToPlaylist(QString PlaylistName, QString Path)
{
    QFile PlFile;
    bool IsCurrent=false;
    QTextStream PlStream;
    PlFile.setFileName("/home/pi/"+PlaylistName);
    PlFile.open(QIODevice::Append);
    PlStream.setDevice(&PlFile);
    PlStream << Path << "\n";
    PlStream.flush();

    if(playlist_file.fileName() == PlFile.fileName())
    {
        music_list->addMedia(QMediaContent(QUrl::fromLocalFile(Path)));
        IsCurrent = true;
    }
    PlFile.close();

    if(PlaylistName == "playlist") AddToFunPlaylist(Path);

    return IsCurrent;
}

void playlist::AddToFunPlaylist(QString PathToTrack)
{
    QFile PlaFile;
    QTextStream PlaStream;
    PlaFile.setFileName("/home/pi/fun_playlist");
    PlaFile.open(QIODevice::Append);
    PlaStream.setDevice(&PlaFile);
    PlaStream << PathToTrack << "\n";
    PlaStream.flush();

    PlaFile.close();
}

void playlist::SearchAndDelete(QString Path)
{
    list_playlists(true);
    playlist_list.push_back("fun_playlist");
    QFile TargetFile;
    QString TempString = Path+"\n";

    for(int x=0; x < playlist_list.size(); x++)
    {
        int Index = 0;
        TargetFile.setFileName("/home/pi/"+playlist_list[x]);
        if(!TargetFile.open(QIODevice::ReadWrite)) continue;

        while(!(TargetFile.atEnd()))
        {
            //qDebug () <<"Temp String: " <<TempString;
            if(TempString == TargetFile.readLine())
            {
                qDebug () <<"Znaleziono!";
                QStringList TempList;
                QTextStream TempStream;
                int pos=0;
                TempStream.setDevice(&TargetFile);
                TargetFile.seek(0);
                while(!(TargetFile.atEnd()))
                {
                    if(pos == Index) TargetFile.readLine();
                    else TempList.push_back(TargetFile.readLine());
                    pos++;
                }
                TargetFile.resize(0);
                for(int z=0; z < TempList.size(); z++)
                {
                    TempStream << TempList[z];
                }

                TempStream.flush();
                TargetFile.seek(0);
                qDebug () <<"CurrentPlaylist: " <<playlist_file.fileName();
                qDebug () <<"TargetPlaylist: " <<TargetFile.fileName();
                if(playlist_file.fileName() == TargetFile.fileName())
                {
                    qDebug () <<"Usuwanie z obecnej plylisty \n";
                    music_list->removeMedia(Index);
                    emit DeleteFromList(Index);
                }

                Index = -1;
            }

            Index++;
        }
        TargetFile.close();

    }
}

void playlist::SetRadioStation(int Index)
{
    music_list->clear();
    music_list->addMedia(QUrl(RadioStationsUrls[Index]));
}

QStringList playlist::LoadSavedRadioStations()
{
    if((!RadioStationsNames.isEmpty()) && (!RadioStationsUrls.isEmpty())) return RadioStationsNames;
    if(!RadioStationsFile.open(QFile::ReadOnly)) return QStringList();

    RadioStationsNames.clear();
    RadioStationsUrls.clear();
    QString TempLine;
    QString TempLine2;

    while(!RadioStationsFile.atEnd())
    {
        TempLine = RadioStationsFile.readLine();
        for(int x=0; x < TempLine.size(); x++)
        {
            if(TempLine[x] == ";")
            {
                RadioStationsNames.push_back(TempLine2);
                TempLine2.clear();
                continue;
            }
            TempLine2 += TempLine[x];
            if(x+2 == TempLine.size()) RadioStationsUrls.push_back(TempLine2);
        }
        TempLine2.clear();
    }
    RadioStationsFile.close();
    return RadioStationsNames;
}

void playlist::AddNewRadioEntry(QString Entry)
{
    if(!RadioStationsFile.open(QIODevice::Append)) return;
    QString TempLine;
    QTextStream TxStream;
    TxStream.setDevice(&RadioStationsFile);
    TxStream <<Entry <<"\n";
    TxStream.flush();
    RadioStationsFile.close();

    for(int x=0; x < Entry.size(); x++)
    {
        if(Entry[x] == ";")
        {
            RadioStationsNames.push_back(TempLine);
            TempLine.clear();
            continue;
        }
        TempLine += Entry[x];
        if(x+1 == Entry.size()) RadioStationsUrls.push_back(TempLine);
    }
}

void playlist::DeleteSelectedRadio(int Index)
{
    QTextStream PlStream;
    QStringList del_buffer;
    if(!RadioStationsFile.open(QIODevice::ReadWrite)) return;
    RadioStationsFile.seek(0);
    PlStream.setDevice(&RadioStationsFile);
    int selected_index=0;
    while(!RadioStationsFile.atEnd())
    {
        if(Index != selected_index) del_buffer.push_back(RadioStationsFile.readLine());
        if(Index == selected_index) RadioStationsFile.readLine();
        selected_index++;
    }

    RadioStationsFile.resize(0);
    for(int z=0;z<del_buffer.size();z++)
    {
        PlStream << del_buffer[z];
    }
    PlStream.flush();
    RadioStationsFile.close();

    RadioStationsNames.removeAt(Index);
    RadioStationsUrls.removeAt(Index);
}

QString playlist::GetRadioFullEntry(int Index)
{
    return QString(RadioStationsNames[Index]+";"+RadioStationsUrls[Index]);
}
