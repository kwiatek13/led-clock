#ifndef CIRCLE_H
#define CIRCLE_H
#include <QMainWindow>
#include <QObject>
#include <QPainter>
#include <QPen>
#include <QVBoxLayout>
#include <QSlider>

class CPBar : public QWidget {
    Q_OBJECT
    qreal p; // progress 0.0 to 1.0
  public:
    QColor color = QColor("#61e02a");
    CPBar(QWidget * p = 0) : QWidget(p), p(0) {
      setMinimumSize(100, 100);


    }
    void upd(qreal pp) {
      if (p == pp) return;
      p = pp;
      update();
    }
  void paintEvent(QPaintEvent *) {
    qreal pd = p * 360;
    qreal rd = 360 - pd;
    QPainter p(this);
   // p.fillRect(rect(), Qt::white);
    p.translate(4, 4);
    p.setRenderHint(QPainter::Antialiasing);
    QPainterPath path, path2;
    path.moveTo(145, 0);
    path.arcTo(QRectF(0, 0, 290, 290), 90, -pd);
    QPen pen, pen2;
    pen.setCapStyle(Qt::FlatCap);
    pen.setColor(color);
    pen.setWidth(3);
    p.strokePath(path, pen);
    path2.moveTo(145, 0);
    pen2.setWidth(3);
    //pen2.setColor(QColor("#d7d7d7"));
    pen2.setColor(QColor("#888A85"));
    pen2.setCapStyle(Qt::FlatCap);
    //pen2.setDashPattern(QVector<qreal>{2.5, 1.105});
    path2.arcTo(QRectF(0, 0, 290, 290), 90, rd);
    //pen2.setDashOffset(2.2);
    p.strokePath(path2, pen2);
  }

  void ChangeColor(QString cl)
  {
      color = QColor(cl);
      update();
  }
};



#endif // CIRCLE_H
