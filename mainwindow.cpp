#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    AlarmsDialog.SetAlarmsTabPointer(&(AlarmsTab));
    LightSettingsWindow.SetPointer(&LightControl);
    ui->gridLayout->addWidget(&Circle);
    Circle.ChangeColor("#61e02a");
    Circle.upd(1);
    AlarmsDialog.LoadAlarmsList();
    AlarmsDialog.SetAlarmDate();
    ClosestAlarm = AlarmsDialog.FindClosestAlarm();
    connect(&TimeChange, SIGNAL(timeout()), this, SLOT(TimeRefresh()));
    connect(&AlarmRemainingTimer, SIGNAL(timeout()), this, SLOT(ShowAlarmRemaining()));
    connect(&AlarmsDialog, SIGNAL(LaunchCloseAlarmSet()), this, SLOT(SetCloseAlarm()));
    connect(&WakeupWindow, SIGNAL(SnoozeSignal()), this, SLOT(SnoozeSlot()));
    connect(&WakeupWindow, SIGNAL(OffSignal()), this, SLOT(OffAlarmSlot()));
    connect(ui->OffSnoozeBtn, SIGNAL(clicked()), this, SLOT(OffAlarmSlot()));
    auto CurrSec = Times.currentDateTime().time().second();
    while(Times.currentDateTime().time().second() != CurrSec+2);
    TimeChange.start(250);
    //ui->SnoozeBtn->setVisible(false);
    ui->OffSnoozeBtn->setVisible(false);
    SnoozeAlarm.SetAsOnce(false);
    SnoozeAlarm.SetEnabled(true);
    AlarmRemainingTimer.setSingleShot(true);
    ShowAlarmRemaining();
    TimeoutTimer.setSingleShot(true);
    connect(&TimeoutTimer, SIGNAL(timeout()), &WakeupWindow, SLOT(on_OffBtn_clicked()));
    AlarmsDialog.ConnectToAlbumWindow(MultimediaWindow.GetAlbumsPtr());

    WakeupWindow.SetPointerToSharedData(&SharedDeviceData);

    DevicesRead = new GPIODevicesRead(&SharedDeviceData);
    connect(&DevicesReadThread, SIGNAL(started()), DevicesRead, SLOT(ReadProcess()));
    connect(DevicesRead, SIGNAL(RCButtonPressed(unsigned long)), this, SLOT(ReceivedRCCode(unsigned long)));
    connect(DevicesRead, SIGNAL(CombinationTyped(int)), this, SLOT(ReceivedRCCombination(int)));
    //connect(&DHTProcess, SIGNAL(finished(int)), this, SLOT(FinishedDHTRead(int)));
    DevicesRead->moveToThread(&DevicesReadThread);
    DevicesReadThread.start();

    ui->LampSettingsButton->setVisible(false);
    TempOutputFile.setFileName("/home/pi/Adafruit_Python_DHT/examples/OutputFile");
    DHTRead();
    //AlarmRemainingTimer.start(60000);

    //LightControl.TestLed();
    //LightControl.LaunchSunrise(15);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::LaunchAlarm()
{
    ui->AlarmLabel->setText("Dryn!");
    //ui->SnoozeBtn->setVisible(true);
    //ui->OffBtn->setVisible(true);
    WakeupWindow.ShowWindow();
   switch(ClosestAlarm->GetAlarmMode())
   {
   case DefaultSignal:
   {
       MultimediaWindow.GetPlaylist()->clear();
       MultimediaWindow.GetPlaylist()->getPlaylist()->addMedia(QMediaContent(QUrl::fromLocalFile(DefAlarmPath)));
       MultimediaWindow.GetPlayer()->odtworz(0);
       break;
   }
   case FunStart:
   {
        MultimediaWindow.GetPlaylist()->fun_start();
        MultimediaWindow.GetPlayer()->odtworz(0);
       break;
   }
   case SelectedTrackMod:
   {
       //Dodac sprawdzanie czy plik istnieje, jesli nie to odtwarzamy default signal (zrobione, do sprawdzenia)
       QFileInfo FileTest;
       QString TempPath;
       FileTest.setFile(ClosestAlarm->GetAlarmValue());
       if(FileTest.exists()) TempPath = ClosestAlarm->GetAlarmValue();
       else TempPath = DefAlarmPath;
       MultimediaWindow.GetPlaylist()->clear();
       MultimediaWindow.GetPlaylist()->getPlaylist()->addMedia(QMediaContent(QUrl::fromLocalFile(TempPath)));
       MultimediaWindow.GetPlayer()->odtworz(0);
       break;
   }

   case Radio:
   {
       //Dodac probe polaczenia z serwerem, jesli sie nie uda to odtwarzamy default signal
       QString RadioURL = ClosestAlarm->GetAlarmValue();
       RadioURL = RadioURL.remove(0, RadioURL.indexOf(";")+1);
       MultimediaWindow.GetPlaylist()->clear();
       MultimediaWindow.GetPlaylist()->getPlaylist()->addMedia(QUrl(RadioURL));
       MultimediaWindow.GetPlayer()->odtworz(0);
       CheckRadioPlayback();
       break;
   }

   }


    MultimediaWindow.GetPlaylist()->getPlaylist()->setPlaybackMode(QMediaPlaylist::CurrentItemInLoop);

    TimeoutTimer.start(TimeoutAlarm);

}


void MainWindow::TimeRefresh()
{
    ui->label->setText(Times.currentDateTime().time().toString("hh:mm"));

    float Val = (Times.currentDateTime().time().second());
    if(Val == 0)
    {
        Circle.upd(1);
    }
    else
    {
        Circle.upd(Val/60);
    }
    if(SharedDeviceData.IsSnoozeButtonPushed)
    {
        if(LaunchLampCounter < 10) LaunchLampCounter++;
        if(LaunchLampCounter == 4)
        {
            if(ui->LampSettingsButton->isVisible())
                ui->LampSettingsButton->setVisible(false);
            else
                ui->LampSettingsButton->setVisible(true);

            LightSettingsWindow.SwitchLamp();
        }

    }
    else if(!SharedDeviceData.IsSnoozeButtonPushed && LaunchLampCounter > 0)
        LaunchLampCounter = 0;

    if(!ClosestAlarm || AlarmActivated) return;
    if(CheckSunrise)
    {
        if((SunriseDate.date() == Times.currentDateTime().date()) && (SunriseDate.time().toString("hh:mm:ss") == Times.currentDateTime().time().toString("hh:mm:ss")))
        {
                LightControl.LaunchSunrise(ClosestAlarm->GetSunriseTime());
                CheckSunrise = false;
                return;
        }
    }
    if(ClosestAlarm->GetOnceDate() == Times.currentDateTime().date())
    {
        if(ClosestAlarm->GetTime().toString("hh:mm:ss") == Times.currentDateTime().time().toString("hh:mm:ss"))
        {
            AlarmActivated=true;
            LaunchAlarm();
            //AlarmSet *TempAl = AlarmsDialog.FindClosestAlarm(); //Trzeba ustawic nowa date dla alarmu!!!!
            //ClosestAlarm.setDate(TempAl->GetOnceDate());
            //ClosestAlarm.setTime(TempAl->GetTime());
            //TempAl->SetEnabled(false);
        }
    }
}

void MainWindow::on_pushButton_clicked()
{
  //  AlarmSetter.LoadAlarmEntry(&Sample);
  //  AlarmSetter.show();
   // AlarmsDialog.showFullScreen();
    AlarmsDialog.show();

    //ClosestAlarm = AlarmsDialog.FindClosestAlarm();
    //ClosestAlarm.setDate(TempAl->GetOnceDate());
    //ClosestAlarm.setTime(TempAl->GetTime());
}

void MainWindow::OffAlarmSlot()
{
    TimeoutTimer.stop();
    if(ClosestAlarm->GetOnceStatus())
    {
        ClosestAlarm->SetEnabled(false);
    }
    else
    {
        AlarmsDialog.SetAlarmDate();
    }
    //ClosestAlarm = AlarmsDialog.FindClosestAlarm();
    SetCloseAlarm();
    AlarmActivated=false;
    //ui->SnoozeBtn->setVisible(false);
    ui->OffSnoozeBtn->setVisible(false);
    ui->AlarmLabel->setText("");
    AlarmsDialog.RefreshButtons();
    AlarmsDialog.SaveToFile();

    MultimediaWindow.GetPlayer()->odtworz(3);
    MultimediaWindow.GetPlaylist()->getPlaylist()->clear();
    MultimediaWindow.GetPlaylist()->getPlaylist()->setPlaybackMode(QMediaPlaylist::Sequential);
    LightControl.OffLed();


}

void MainWindow::SnoozeSlot()
{
    TimeoutTimer.stop();
    if(ClosestAlarm->GetOnceStatus()) ClosestAlarm->SetEnabled(false);
    SnoozeAlarm.SetAlarmMode(ClosestAlarm->GetAlarmMode());
    SnoozeAlarm.SetAlarmValue(ClosestAlarm->GetAlarmValue());
    ClosestAlarm = &SnoozeAlarm;
    //ClosestAlarm->SetEnabled(true);
    //QTime TempTime = ClosestAlarm->GetTime().addSecs(SnoozeTime);
    QDateTime TempTime=QDateTime::currentDateTime();
    //TempTime.setDate(ClosestAlarm->GetOnceDate());
    //TempTime.setTime(ClosestAlarm->GetTime());
    TempTime = TempTime.addSecs(SnoozeTime);
    ClosestAlarm->SetHour(TempTime.time().hour(),TempTime.time().minute());
    ClosestAlarm->SetSecs(TempTime.time().second());
    ClosestAlarm->SetOnceDate(TempTime.date().day(),TempTime.date().month(), TempTime.date().year());
    //ClosestAlarm = AlarmsDialog.FindClosestAlarm();
    AlarmActivated=false;
    //ui->SnoozeBtn->setVisible(false);
    ui->OffSnoozeBtn->setVisible(true);
    ui->AlarmLabel->setText("Zzz");

    MultimediaWindow.GetPlayer()->odtworz(3);
    MultimediaWindow.GetPlaylist()->getPlaylist()->clear();
    MultimediaWindow.GetPlaylist()->getPlaylist()->setPlaybackMode(QMediaPlaylist::Sequential);
    //LightControl.OffLed();

}

void MainWindow::SetCloseAlarm()
{
    ClosestAlarm = AlarmsDialog.FindClosestAlarm();
    AlarmRemainingTimer.stop();
    ShowAlarmRemaining();

    if(!ClosestAlarm)
    {
        CheckSunrise = false;
    }
    else if(ClosestAlarm->GetIsSunriseEnabled())
    {
        CheckSunrise = true;
        QDateTime TempTime;
        TempTime.setTime(ClosestAlarm->GetTime());
        TempTime.setDate(ClosestAlarm->GetOnceDate());

        SunriseDate = TempTime.addSecs(-1*ClosestAlarm->GetSunriseTime());
    }
    else CheckSunrise = false;
}

void MainWindow::ShowAlarmRemaining()
{
    if(!ClosestAlarm)
    {
        ui->AlarmRemainingLabel->setText("-");
        AlarmRemainingTimer.start(60000);
        return;
    }

    QDateTime CurrentDate = QDateTime::currentDateTime();
    QDateTime ClosestDate;
    ClosestDate.setDate(ClosestAlarm->GetOnceDate());
    ClosestDate.setTime(ClosestAlarm->GetTime());

    if(CurrentDate.secsTo(ClosestDate) > 86400)
    {
        ui->AlarmRemainingLabel->setText(ClosestDate.toString("dd-MM-yyyy hh:mm"));
        AlarmRemainingTimer.start(60000);
    }
    else if(CurrentDate.secsTo(ClosestDate) > 120)
    {
        int HoursToAlarm = CurrentDate.secsTo(ClosestDate)/3600;
        int MinutesToAlarm = (CurrentDate.secsTo(ClosestDate)-HoursToAlarm*3600)/60;
        if(HoursToAlarm > 0) ui->AlarmRemainingLabel->setText(QString("Pozostalo %1 godz. %2 min.").arg(HoursToAlarm).arg(MinutesToAlarm));
        else ui->AlarmRemainingLabel->setText(QString("Pozostalo %1 min.").arg(MinutesToAlarm));
        AlarmRemainingTimer.start(60000);
    }
    else if(CurrentDate.secsTo(ClosestDate) < 120 && CurrentDate.secsTo(ClosestDate) > 0)
    {
        int SecondsToAlarm = CurrentDate.secsTo(ClosestDate);
        if(SecondsToAlarm >= 60) ui->AlarmRemainingLabel->setText(QString("Pozostalo 1 min."));
        else ui->AlarmRemainingLabel->setText(QString("Pozostalo %1s").arg(SecondsToAlarm));
        AlarmRemainingTimer.start(1000);
    }
    else
    {
        ui->AlarmRemainingLabel->setText("-");
        AlarmRemainingTimer.start(1000);
    }

    //fprintf(stderr, "Refresh!\n");
}

void MainWindow::on_MultimediaButton_clicked()
{
    MultimediaWindow.show();
}

//Do sprawdzenia!!!
void MainWindow::CheckRadioPlayback()
{
    //if(MultimediaWindow.GetPlayer()->getPlayer()->state() == QMediaPlayer::PlayingState) return;
    if(MultimediaWindow.GetPlayer()->getPlayer()->isAudioAvailable() || MultimediaWindow.GetPlayer()->getPlayer()->state() == QMediaPlayer::StoppedState) return;
    else
    {
        qDebug () <<"Fail! \n";
        FailCounter++;
    }

    if(FailCounter > 30)
    {
        if(AlarmActivated)
        {
            MultimediaWindow.GetPlaylist()->clear();
            MultimediaWindow.GetPlaylist()->getPlaylist()->addMedia(QMediaContent(QUrl::fromLocalFile(DefAlarmPath)));
            MultimediaWindow.GetPlayer()->odtworz(0);
            MultimediaWindow.GetPlaylist()->getPlaylist()->setPlaybackMode(QMediaPlaylist::CurrentItemInLoop);
        }
        FailCounter = 0;

    }
    else QTimer::singleShot(2000, this, SLOT(CheckRadioPlayback()));
}

void MainWindow::on_LampSettingsButton_clicked()
{
    LightSettingsWindow.show();
    //LightSettingsWindow.EnableLamp();
}

void MainWindow::RefreshDHTRead()
{
    //qDebug () <<"Read!";
    if(!TempOutputFile.isOpen()) TempOutputFile.open(QIODevice::ReadOnly);
    if(TempOutputFile.isOpen())
    {
        QString Output = TempOutputFile.readLine();
        if(Output.length() >= 9)
        {
            QStringList List = Output.split(" ");
            if(List.size() == 2)
            {
                //qDebug () <<"Temp: " <<List[0] <<"Humidity: " <<List[1] <<"\n";
                ui->TemperatureLabel->setText("T: "+List[0]+"℃");
                ui->HumidityLabel->setText("H: "+List[1]+"%");
            }


        }
        TempOutputFile.seek(0);
        TempOutputFile.close();
    }
    //QString Output = DHTProcess.readAllStandardOutput();


   QTimer::singleShot(20000, this, SLOT(RefreshDHTRead()));
}

void MainWindow::DHTRead()
{
    DHTProcess.start("python", QStringList () <<"/home/pi/Adafruit_Python_DHT/examples/AdafruitDHT.py" <<"22" <<"22");
    RefreshDHTRead();
}

void MainWindow::ReceivedRCCode(unsigned long CurrentCode)
{
    qDebug () <<"Get code: " <<CurrentCode <<"\n";
}

void MainWindow::ReceivedRCCombination(int CurrentCombination)
{
    qDebug () <<"Get combination: " <<CurrentCombination <<"\n";
}
