#ifndef MULTIMEDIAPLAYERWINDOW_H
#define MULTIMEDIAPLAYERWINDOW_H

#include <QDialog>
#include "player.h"
#include "playlist.h"
#include "playerwindow.h"
#include "playlistswindow.h"
#include "albumslibrary.h"
#include "filesmanager.h"
#include <QMessageBox>

namespace Ui {
class MultimediaPlayerWindow;
}

class MultimediaPlayerWindow : public QDialog
{
    Q_OBJECT

public:
    explicit MultimediaPlayerWindow(QWidget *parent = 0);
    ~MultimediaPlayerWindow();
    player *GetPlayer();
    playlist *GetPlaylist();
    AlbumsLibrary *GetAlbumsPtr();

private slots:
    void on_MusicButton_clicked();

    void on_BackButton_clicked();

    void on_PlaylistButton_clicked();

    void on_TestButton_clicked();

    void RefreshTime();

    void on_InternetRadioButton_clicked();

private:
    Ui::MultimediaPlayerWindow *ui;
    player LocalPlayer;
    //playlist LocalPlaylists;
    playlist LocalPlaylist;
    PlayerWindow LocalPlayerWindow;
    PlaylistsWindow LocalPlaylistWindow;
    AlbumsLibrary LocalAlbumsWindow;
    FilesManager LocalFilesManagerWindow;
    QMessageBox MSgBox;

public slots:
    void PlayerCall();
};

#endif // MULTIMEDIAPLAYERWINDOW_H
