#include "alarmsetdialog.h"
#include "ui_alarmsetdialog.h"

AlarmSetDialog::AlarmSetDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AlarmSetDialog)
{
    ui->setupUi(this);
    connect(ui->AcceptButton, SIGNAL(clicked()), this, SLOT(SaveAlarm()));
 //   connect(ui->AlbumButton, SIGNAL(clicked()), this, SIGNAL(OpenSetWindow()));
}

AlarmSetDialog::~AlarmSetDialog()
{
    delete ui;
}

void AlarmSetDialog::on_MoreHour_clicked()
{
    ui->HoursEdit->setText(QString::number((ui->HoursEdit->text().toInt())+1));
}

void AlarmSetDialog::on_LessHour_clicked()
{
    ui->HoursEdit->setText(QString::number((ui->HoursEdit->text().toInt())-1));
}

void AlarmSetDialog::on_MoreMinutes_clicked()
{
    ui->MinutesEdit->setText(QString::number((ui->MinutesEdit->text().toInt())+1));
}

void AlarmSetDialog::on_LessMinutes_clicked()
{
    ui->MinutesEdit->setText(QString::number((ui->MinutesEdit->text().toInt())-1));
}



void AlarmSetDialog::on_MoreMonths_clicked()
{
    ui->MonthsEdit->setText(QString::number((ui->MonthsEdit->text().toInt())+1));
}

void AlarmSetDialog::on_LessMonths_clicked()
{
    ui->MonthsEdit->setText(QString::number((ui->MonthsEdit->text().toInt())-1));
}

void AlarmSetDialog::on_MoreDays_clicked()
{
    ui->DaysEdit->setText(QString::number((ui->DaysEdit->text().toInt())+1));
}

void AlarmSetDialog::on_MoreYears_clicked()
{
    ui->YearsEdit->setText(QString::number((ui->YearsEdit->text().toInt())+1));
}

void AlarmSetDialog::on_LessDays_clicked()
{
    ui->DaysEdit->setText(QString::number((ui->DaysEdit->text().toInt())-1));
}

void AlarmSetDialog::on_LessYears_clicked()
{
    ui->YearsEdit->setText(QString::number((ui->YearsEdit->text().toInt())-1));
}

void AlarmSetDialog::on_BackButton_clicked()
{
    emit RefreshStates();
    close();
}

void AlarmSetDialog::LoadAlarmEntry(AlarmSet *point)
{
    if(!point) return;
    AlarmPointer = point;
    PathForTrack.clear();
    URLForRadio.clear();


    int TempAlrMod = AlarmPointer->GetAlarmMode();
    if(TempAlrMod != SelectedTrackMod && TempAlrMod != Radio)
    {
        ui->TrackLabel->setVisible(false);
        ui->AlbumButton->setVisible(false);
        ui->SelectedTrackLabel->setVisible(false);
    }

    if(TempAlrMod == SelectedTrackMod) PathForTrack = AlarmPointer->GetAlarmValue();
    else if(TempAlrMod == Radio) URLForRadio = AlarmPointer->GetAlarmValue();

    ui->HoursEdit->setText(AlarmPointer->GetTime().toString("hh"));
    ui->MinutesEdit->setText(AlarmPointer->GetTime().toString("mm"));
    ui->MonthsEdit->setText(AlarmPointer->GetOnceDate().toString("MM"));
    ui->YearsEdit->setText(AlarmPointer->GetOnceDate().toString("yyyy"));
    ui->DaysEdit->setText(AlarmPointer->GetOnceDate().toString("dd"));
    if(AlarmPointer->GetEnabledStatus()) ui->EnabledCheckBox->setChecked(true);
    else ui->EnabledCheckBox->setChecked(false);
    ui->OnceCheckBox->click();
    if(AlarmPointer->GetOnceStatus()) ui->OnceCheckBox->setChecked(true);
    else ui->OnceCheckBox->setChecked(false);

    ui->MoncheckBox->setChecked(AlarmPointer->GetDayState(Monday));
    ui->TuecheckBox->setChecked(AlarmPointer->GetDayState(Tuesday));
    ui->WedcheckBox->setChecked(AlarmPointer->GetDayState(Wendesday));
    ui->ThucheckBox->setChecked(AlarmPointer->GetDayState(Thursday));
    ui->FricheckBox->setChecked(AlarmPointer->GetDayState(Friday));
    ui->SatcheckBox->setChecked(AlarmPointer->GetDayState(Saturday));
    ui->SuncheckBox->setChecked(AlarmPointer->GetDayState(Sunday));

    ui->SignalCombo->setCurrentIndex(TempAlrMod);

    ui->SunriseCheckBox->click();
    if(AlarmPointer->GetIsSunriseEnabled()) ui->SunriseCheckBox->setChecked(true);
    else ui->SunriseCheckBox->setChecked(false);

    ui->SunriseEdit->setText(QString::number(AlarmPointer->GetSunriseTime()/60));
   // ui->TrackLabel->se

    //showFullScreen();
    show();

}

void AlarmSetDialog::GetAlrValue(QString Val)
{
    //Dodac pobieranie nazwy i url do radia

    if(ui->SignalCombo->currentIndex() == Radio)
    {
        URLForRadio = Val;
        QString TempTitle = URLForRadio;
        TempTitle.remove(TempTitle.indexOf(";"), TempTitle.size()-TempTitle.indexOf(";"));
        ui->SelectedTrackLabel->setText(TempTitle);
    }
    else
    {
        QFileInfo Tempinfo;
        PathForTrack = Val;
        Tempinfo.setFile(PathForTrack);
        ui->SelectedTrackLabel->setText(Tempinfo.completeBaseName());
    }
}

void AlarmSetDialog::on_HoursEdit_textChanged(const QString &arg1)
{
    auto Val = ui->HoursEdit->text().toInt();
    if(Val < 0) ui->HoursEdit->setText(QString::number(23));
    if(Val > 23) ui->HoursEdit->setText(QString::number(0));
}

void AlarmSetDialog::on_MinutesEdit_textChanged(const QString &arg1)
{
    auto Val = ui->MinutesEdit->text().toInt();
    if(Val < 0) ui->MinutesEdit->setText(QString::number(59));
    if(Val > 59) ui->MinutesEdit->setText(QString::number(0));
}

void AlarmSetDialog::on_YearsEdit_textChanged(const QString &arg1)
{
    auto Val = ui->YearsEdit->text().toInt();
    QDate TempDat;
    TempDat.setDate(Val ,ui->MonthsEdit->text().toInt(), 1);
    if(ui->DaysEdit->text().toInt() > TempDat.daysInMonth()) ui->DaysEdit->setText(QString::number(1));
    SetDayToDate();
}

void AlarmSetDialog::on_MonthsEdit_textChanged(const QString &arg1)
{
    int Val = ui->MonthsEdit->text().toInt();
    QDate TempDat;
    // if(Val > 12) ui->MonthsEdit->setText(QString::number(1));    //Do weryfikacji!!
    if(Val < 1) ui->MonthsEdit->setText(QString::number(12));
    else ui->MonthsEdit->setText(QString::number(Val));
    TempDat.setDate(ui->YearsEdit->text().toInt() ,ui->MonthsEdit->text().toInt(), 1);
    if(ui->DaysEdit->text().toInt() > TempDat.daysInMonth()) ui->DaysEdit->setText(QString::number(1));
    SetDayToDate();
}

void AlarmSetDialog::on_DaysEdit_textChanged(const QString &arg1)
{
    auto Val = ui->DaysEdit->text().toInt();
    QDate TempDat;
    TempDat.setDate(ui->YearsEdit->text().toInt() ,ui->MonthsEdit->text().toInt(), 1);
    if(Val > TempDat.daysInMonth()) ui->DaysEdit->setText(QString::number(1));
    if(Val < 1) ui->DaysEdit->setText(QString::number(TempDat.daysInMonth()));
    SetDayToDate();
}

void AlarmSetDialog::on_OnceCheckBox_stateChanged(int arg1)
{
    if(ui->OnceCheckBox->isChecked())
    {
        ui->MoreDays->setVisible(true);
        ui->MoreMonths->setVisible(true);
        ui->MoreYears->setVisible(true);
        ui->LessDays->setVisible(true);
        ui->LessMonths->setVisible(true);
        ui->LessYears->setVisible(true);
        ui->MonthsEdit->setVisible(true);
        ui->DaysEdit->setVisible(true);
        ui->YearsEdit->setVisible(true);

        ui->MoncheckBox->setVisible(false);
        ui->TuecheckBox->setVisible(false);
        ui->WedcheckBox->setVisible(false);
        ui->ThucheckBox->setVisible(false);
        ui->FricheckBox->setVisible(false);
        ui->SatcheckBox->setVisible(false);
        ui->SuncheckBox->setVisible(false);
        ui->DayLabel->setVisible(true);
    }
    else
    {
        ui->MoreDays->setVisible(false);
        ui->MoreMonths->setVisible(false);
        ui->MoreYears->setVisible(false);
        ui->LessDays->setVisible(false);
        ui->LessMonths->setVisible(false);
        ui->LessYears->setVisible(false);
        ui->MonthsEdit->setVisible(false);
        ui->DaysEdit->setVisible(false);
        ui->YearsEdit->setVisible(false);

        ui->MoncheckBox->setVisible(true);
        ui->TuecheckBox->setVisible(true);
        ui->WedcheckBox->setVisible(true);
        ui->ThucheckBox->setVisible(true);
        ui->FricheckBox->setVisible(true);
        ui->SatcheckBox->setVisible(true);
        ui->SuncheckBox->setVisible(true);
        ui->DayLabel->setVisible(false);
    }
}

void AlarmSetDialog::SaveAlarm()
{
    bool AnyDay=false;
    int TempAlrMode = ui->SignalCombo->currentIndex();
    AlarmPointer->SetHour(ui->HoursEdit->text().toInt(), ui->MinutesEdit->text().toInt());
    AlarmPointer->SetOnceDate(ui->DaysEdit->text().toInt(), ui->MonthsEdit->text().toInt(), ui->YearsEdit->text().toInt());
    AlarmPointer->SetAsOnce(ui->OnceCheckBox->isChecked());
    AlarmPointer->SetEnabled(ui->EnabledCheckBox->isChecked());
    AlarmPointer->SetDays(Monday, ui->MoncheckBox->isChecked());
    AlarmPointer->SetDays(Tuesday, ui->TuecheckBox->isChecked());
    AlarmPointer->SetDays(Wendesday, ui->WedcheckBox->isChecked());
    AlarmPointer->SetDays(Thursday, ui->ThucheckBox->isChecked());
    AlarmPointer->SetDays(Friday, ui->FricheckBox->isChecked());
    AlarmPointer->SetDays(Saturday, ui->SatcheckBox->isChecked());
    AlarmPointer->SetDays(Sunday, ui->SuncheckBox->isChecked());
    if((PathForTrack.isEmpty() && TempAlrMode == SelectedTrackMod) || (URLForRadio.isEmpty() && TempAlrMode == Radio)) TempAlrMode = DefaultSignal;
    AlarmPointer->SetAlarmMode(TempAlrMode);
    if(TempAlrMode == SelectedTrackMod) AlarmPointer->SetAlarmValue(PathForTrack);
    else if(TempAlrMode == Radio) AlarmPointer->SetAlarmValue(URLForRadio);
    AlarmPointer->SetSunriseEnabled(ui->SunriseCheckBox->isChecked());
    AlarmPointer->SetSunriseTime(ui->SunriseEdit->text().toInt()*60);
    for(int x=0; x <= 6; x++) if(AlarmPointer->GetDayState(x)) AnyDay=true;
    if(!AnyDay && !AlarmPointer->GetOnceStatus()) AlarmPointer->SetEnabled(false);
    emit RefreshStates();
    emit SaveData();
    close();
}

void AlarmSetDialog::SetDayToDate()
{
    int DayOfWeek = QDate(ui->YearsEdit->text().toInt(), ui->MonthsEdit->text().toInt(), ui->DaysEdit->text().toInt()).dayOfWeek();
    QString DayName;

    switch(DayOfWeek)
    {
    case 1:
        DayName = "Poniedzialek";
        break;

    case 2:
        DayName = "Wtorek";
        break;

    case 3:
        DayName = "Sroda";
        break;

    case 4:
        DayName = "Czwartek";
        break;

    case 5:
        DayName = "Piatek";
        break;

    case 6:
        DayName = "Sobota";
        break;

    case 7:
        DayName = "Niedziela";
        break;

    }

    ui->DayLabel->setText(DayName);
}

void AlarmSetDialog::ShowSelectedTrackMode()
{
    QString Result; //= AlarmPointer->GetAlarmValue();
 //   PopWindow.SetAsSelectOptionWindow("Wybierz stację radiową:");

    if(!(PathForTrack.isEmpty()))
    {
        QFileInfo TrackInfo;
        TrackInfo.setFile(PathForTrack);
        //PathForTrack = Result;
        Result = TrackInfo.completeBaseName();
    }
    else Result = "Nie ustawiono!";

    ui->SelectedTrackLabel->setText(Result);
    ui->AlbumButton->setText("Wybeirz utwór");
    ui->TrackLabel->setText("Obecnie wybrany:");
    ui->TrackLabel->setVisible(true);
    ui->AlbumButton->setVisible(true);
    ui->SelectedTrackLabel->setVisible(true);
}

void AlarmSetDialog::ShowSelectedRadioMode()
{
    QString Result; //= AlarmPointer->GetAlarmValue();

    if(!(URLForRadio.isEmpty()))
    {
        int TempPos=0;
        while(URLForRadio[TempPos] != ";" && URLForRadio.size() > TempPos)
        {
            Result += URLForRadio[TempPos];
            TempPos++;
        }
    }
    else Result = "Nie ustawiono!";

    ui->SelectedTrackLabel->setText(Result);
    ui->AlbumButton->setText("Wybierz stację");
    ui->TrackLabel->setText("Obecnie wybrana:");
    ui->TrackLabel->setVisible(true);
    ui->AlbumButton->setVisible(true);
    ui->SelectedTrackLabel->setVisible(true);
}

void AlarmSetDialog::on_SignalCombo_currentIndexChanged(int index)
{
    if(index == SelectedTrackMod) ShowSelectedTrackMode();
    else if(index == Radio) ShowSelectedRadioMode();
    else
    {
        ui->TrackLabel->setVisible(false);
        ui->AlbumButton->setVisible(false);
        ui->SelectedTrackLabel->setVisible(false);
    }
}

void AlarmSetDialog::on_AlbumButton_clicked()
{
    //Przycisk sie nie pojawia po ponownym otwarciu okna alarmu w trybie radio, gdy bylo ustawione
    //Nie pokazuje ustawioengo utworu po ponownym wejsciu w okno
    emit OpenSetWindow(ui->SignalCombo->currentIndex());
}

void AlarmSetDialog::on_SunriseCheckBox_stateChanged(int arg1)
{
    if(ui->SunriseCheckBox->isChecked())
    {
        ui->SunriseEdit->setVisible(true);
        ui->SunriseLabel->setVisible(true);
        ui->MoreSunrise->setVisible(true);
        ui->LessSunrise->setVisible(true);
    }
    else
    {
        ui->SunriseEdit->setVisible(false);
        ui->SunriseLabel->setVisible(false);
        ui->MoreSunrise->setVisible(false);
        ui->LessSunrise->setVisible(false);
    }
}

void AlarmSetDialog::on_MoreSunrise_clicked()
{
    int TempSunVal = ui->SunriseEdit->text().toInt();
    if(TempSunVal >= 960) return;
    ui->SunriseEdit->setText(QString::number(TempSunVal+5));
}

void AlarmSetDialog::on_LessSunrise_clicked()
{
    int TempSunVal = ui->SunriseEdit->text().toInt();
    if(TempSunVal <= 5) return;
    ui->SunriseEdit->setText(QString::number(TempSunVal-5));
}
