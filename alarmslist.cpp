#include "alarmslist.h"
#include "ui_alarmslist.h"


AlarmsList::AlarmsList(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AlarmsList)
{
    ui->setupUi(this);
    connect(&AlarmSetter,SIGNAL(RefreshStates()), this, SLOT(RefreshButtons()));
    connect(&AlarmSetter, SIGNAL(SaveData()), this, SLOT(SaveToFile()));
    ui->pushButton->setMaximumHeight(60);
    ui->pushButton->setMaximumWidth(60);
    ui->pushButton->setMinimumHeight(60);
    ui->pushButton->setMinimumWidth(60);
    AlarmsFile.setFileName("/home/pi/Alarms.bin");
}

AlarmsList::~AlarmsList()
{
    delete ui;
}

void AlarmsList::AddButtons(int indx)
{
    /* button_tab.push_back(new QPushButton);

     ui->gridLayout->addWidget(button_tab[pos],x,y);
     button_tab[pos]->setMaximumHeight(60);
     button_tab[pos]->setMaximumWidth(60);

    connect(button_tab[pos],&QPushButton::clicked,[=](){set_album_page(filepath);}); //przypisywanie funkcji do przyciskow

   if(!image.isNull())  button_tab[pos]->setIcon(image);


     x++;
     pos++;
     if(x == max_columns)
     {
         x=0;
         y++;
     } */
    auto p = (*AlarmsTabPointer)[indx];
    ButtonsTab.push_back(new QPushButton);
    ButtonsTab.last()->setText(p->GetTime().toString("hh:mm"));

    ui->gridLayout->addWidget(ButtonsTab.last(),y,x);
    ButtonsTab.last()->setMaximumHeight(60);
    ButtonsTab.last()->setMaximumWidth(60);
    //auto Indx = ButtonsTab.size()-1;
    connect(ButtonsTab.last(), &QPushButton::clicked, [=](){ButtonsHandler(indx);});

    x++;
    counter++;

    if(x == max_columns)
    {
        x=0;
        y++;
    }

    ui->gridLayout->addWidget(ui->pushButton,y,x);
/*if(x+1 == max_columns) ui->gridLayout->addWidget(ui->pushButton,0,y+1);
else ui->gridLayout->addWidget(ui->pushButton,x+1,y); */


}

void AlarmsList::SetAlarmsTabPointer(QVector<AlarmSet *> *p)
{
    AlarmsTabPointer = p;
    LoadFromFile();
}

void AlarmsList::LoadAlarmsList()
{
    for(int x=0; x < AlarmsTabPointer->size(); x++)
    {
        AddButtons(x);
    }
    RefreshButtons();
}
void AlarmsList::DeleteAlarm(int pos)
{
    int tempX, tempY;
    // delete ButtonsTab[pos];
    //  ButtonsTab.remove(pos);
    delete (*AlarmsTabPointer)[pos];
    AlarmsTabPointer->remove(pos);
    delete ButtonsTab[pos];
    ButtonsTab.remove(pos);
    for(int i=pos; i < ButtonsTab.size(); i++)
    {
        disconnect(ButtonsTab[i],0,0,0);
        connect(ButtonsTab[i], &QPushButton::clicked, [=](){ButtonsHandler(i);});

        tempY = i/max_columns;
        tempX = i - (tempY*max_columns);
        ui->gridLayout->addWidget(ButtonsTab[i],tempY,tempX);
    }


    if(x == 0)
    {
        y--;
        x = max_columns-1;
    }
    else
    {
        x--;
    }

ui->gridLayout->addWidget(ui->pushButton,y,x);

SaveToFile();
    /*    x=0;
    y=0;
    for(int j=0; j < ButtonsTab.size(); j++)
    {
        ui->gridLayout->addWidget(ButtonsTab[j],x,y);
        x++;


        if(x == max_columns)
        {
            x=0;
            y++;
        }
    } */

    //   RefreshButtons();
    /*
    for(int i=0; i < ButtonsTab.size(); i++)
    {
        delete ButtonsTab[i];
    }
    ButtonsTab.clear();
    counter=0;
    x=0;
    y=0;
    LoadAlarmsList(); */
}

void AlarmsList::AddAlarm()
{
    AlarmsTabPointer->push_back(new AlarmSet);
    AddButtons(AlarmsTabPointer->size()-1);
    // AlarmSetter.LoadAlarmEntry((*AlarmsTabPointer)[AlarmsTabPointer->size()-1]);
    RefreshButtons();
}

void AlarmsList::on_pushButton_clicked()
{
    AddAlarm();
}

void AlarmsList::ButtonsHandler(int index)
{
    if(DeleteMode)
    {
        DeleteAlarm(index);
        DeleteMode=false;
        RefreshButtons();
    }
    else
        AlarmSetter.LoadAlarmEntry((*AlarmsTabPointer)[index]);
}

void AlarmsList::on_pushButton_2_clicked()
{
    if(DeleteMode)
    {
        DeleteMode=false;
        RefreshButtons();
    }

    else
    {
        DeleteMode=true;
        for(int x=0; x < ButtonsTab.size(); x++)
        {
            ButtonsTab[x]->setStyleSheet("\nborder: 1px solid red");

        }
    }
}

void AlarmsList::RefreshButtons()
{
    for(int x=0; x < ButtonsTab.size(); x++)
    {
        ButtonsTab[x]->setText((*AlarmsTabPointer)[x]->GetTime().toString("hh:mm"));
        if((*AlarmsTabPointer)[x]->GetEnabledStatus()) ButtonsTab[x]->setStyleSheet("\nborder: 1px solid white;\nborder-radius: 9px;\nbackground-color: green;\n");
        else ButtonsTab[x]->setStyleSheet("\nborder: 1px solid white;\nborder-radius: 9px;\nbackground-color: red;\n");
    }
}

void AlarmsList::on_pushButton_3_clicked()
{
    emit LaunchCloseAlarmSet();
    close();
}

void AlarmsList::SaveToFile()
{
    QTextStream TextStream;
    if(!AlarmsFile.open(QFile::WriteOnly | QFile::Truncate)) return;
    TextStream.setDevice(&AlarmsFile);

    for(int x=0; x < AlarmsTabPointer->size(); x++)
    {
        TextStream <<(*AlarmsTabPointer)[x]->GetDayState(Monday)<< "\n"<<(*AlarmsTabPointer)[x]->GetDayState(Tuesday)<< "\n"<<(*AlarmsTabPointer)[x]->GetDayState(Wendesday)<< "\n"<<(*AlarmsTabPointer)[x]->GetDayState(Thursday)<< "\n"<<(*AlarmsTabPointer)[x]->GetDayState(Friday)<< "\n"<<(*AlarmsTabPointer)[x]->GetDayState(Saturday)<< "\n"<<(*AlarmsTabPointer)[x]->GetDayState(Sunday)<< "\n";
        TextStream <<(*AlarmsTabPointer)[x]->GetEnabledStatus()<< "\n";
        TextStream <<(*AlarmsTabPointer)[x]->GetOnceDate().day() << "\n" <<(*AlarmsTabPointer)[x]->GetOnceDate().month() << "\n" <<(*AlarmsTabPointer)[x]->GetOnceDate().year() << "\n";
        TextStream <<(*AlarmsTabPointer)[x]->GetOnceStatus()<< "\n";
        TextStream <<(*AlarmsTabPointer)[x]->GetTime().hour() << "\n" <<(*AlarmsTabPointer)[x]->GetTime().minute() << "\n";
        TextStream <<(*AlarmsTabPointer)[x]->GetAlarmMode() << "\n" <<(*AlarmsTabPointer)[x]->GetAlarmValue() <<"\n";
        TextStream <<(*AlarmsTabPointer)[x]->GetIsSunriseEnabled() << "\n" <<(*AlarmsTabPointer)[x]->GetSunriseTime() <<"\n";
        TextStream <<"\n";
    }

    AlarmsFile.close();
    SetAlarmDate();
    //FindClosestAlarm();
}


void AlarmsList::LoadFromFile()
{
    int Mon,Tue,Wen,Thu,Fri,Sat,Sun,IsEn,IsOnc,Hou,Mi,DayOn,MonthOn,YearOn, AlrMod, IsSun, SunTime;
    QString AlrVal;
    if(!AlarmsFile.open(QFile::ReadOnly)) return;

    while(!AlarmsFile.atEnd())
    {
        Mon = AlarmsFile.readLine().toInt();
        Tue = AlarmsFile.readLine().toInt();
        Wen = AlarmsFile.readLine().toInt();
        Thu = AlarmsFile.readLine().toInt();
        Fri = AlarmsFile.readLine().toInt();
        Sat = AlarmsFile.readLine().toInt();
        Sun = AlarmsFile.readLine().toInt();
        IsEn = AlarmsFile.readLine().toInt();
        DayOn = AlarmsFile.readLine().toInt();
        MonthOn = AlarmsFile.readLine().toInt();
        YearOn = AlarmsFile.readLine().toInt();
        IsOnc  = AlarmsFile.readLine().toInt();
        Hou = AlarmsFile.readLine().toInt();
        Mi = AlarmsFile.readLine().toInt();
        AlrMod = AlarmsFile.readLine().toInt();
        AlrVal = AlarmsFile.readLine();
        AlrVal.remove("\n");
        IsSun = AlarmsFile.readLine().toInt();
        SunTime = AlarmsFile.readLine().toInt();
        AlarmsFile.readLine();



        AlarmsTabPointer->push_back(new AlarmSet);
        AlarmsTabPointer->last()->SetDays(Monday,Mon);
        AlarmsTabPointer->last()->SetDays(Tuesday,Tue);
        AlarmsTabPointer->last()->SetDays(Wendesday,Wen);
        AlarmsTabPointer->last()->SetDays(Thursday,Thu);
        AlarmsTabPointer->last()->SetDays(Friday,Fri);
        AlarmsTabPointer->last()->SetDays(Saturday,Sat);
        AlarmsTabPointer->last()->SetDays(Sunday,Sun);
        AlarmsTabPointer->last()->SetEnabled(IsEn);
        AlarmsTabPointer->last()->SetOnceDate(DayOn,MonthOn,YearOn);
        AlarmsTabPointer->last()->SetAsOnce(IsOnc);
        AlarmsTabPointer->last()->SetHour(Hou,Mi);
        AlarmsTabPointer->last()->SetAlarmMode(AlrMod);
        AlarmsTabPointer->last()->SetAlarmValue(AlrVal);
        AlarmsTabPointer->last()->SetSunriseEnabled(IsSun);
        AlarmsTabPointer->last()->SetSunriseTime(SunTime);
        //AlarmsFile.readLine();
    }

    AlarmsFile.close();
}

AlarmSet *AlarmsList::FindClosestAlarm()
{
    AlarmSet* TempAlarm;
    QDateTime CurDat = QDateTime::currentDateTime();
    QDateTime TempDat;
    if(AlarmsTabPointer->size() < 1) return nullptr;
    TempAlarm = (*AlarmsTabPointer)[0];
 /*   if(!TempAlarm->GetOnceStatus())
    {
        TempAlarm.last()->SetOnceDate(CurDat.date().day(),CurDat.date().month(),CurDat.date().year());
    }   */

    for(int x=1; x < AlarmsTabPointer->size(); x++)
    {
        AlarmSet *DateItem = (*AlarmsTabPointer)[x];
        //if(!DateItem->GetOnceStatus()) DateItem->SetOnceDate(CurDat.date().day(),CurDat.date().month(),CurDat.date().year());
        if(!DateItem->GetEnabledStatus()) continue;
            if(TempAlarm->GetOnceDate() > DateItem->GetOnceDate() || !TempAlarm->GetEnabledStatus())
            {
                TempAlarm = DateItem;
            }
            else if(TempAlarm->GetOnceDate() == DateItem->GetOnceDate())
            {
                if(DateItem->GetTime() < TempAlarm->GetTime()) TempAlarm = DateItem;
            }

    }

    qDebug("Closest time: ");
    qDebug(TempAlarm->GetOnceDate().toString("dd.MM.yyyy").toUtf8());
    qDebug(TempAlarm->GetTime().toString("hh:mm").toUtf8());

    if(TempAlarm->GetEnabledStatus()) return TempAlarm;
    else return nullptr;
}

void AlarmsList::SetAlarmDate()
{
    QDate CurDat = QDate::currentDate();
    QDate ResultDat=CurDat;
    int CurrentDayOfWeek = (CurDat.dayOfWeek())-1;
    int DaysOffset=0;
    QTime SetTime;
    QTime CurrentTime = QTime::currentTime();
    bool CheckNext = false;

    for(int x=0; x < AlarmsTabPointer->size();x++)
    {
        bool IsFromBeggining=false;
        SetTime = (*AlarmsTabPointer)[x]->GetTime();
        if((*AlarmsTabPointer)[x]->GetEnabledStatus() && !(*AlarmsTabPointer)[x]->GetOnceStatus())
        {

            for(int a=CurrentDayOfWeek; a < 7; a++)
            {
                if((*AlarmsTabPointer)[x]->GetDayState(a))
                {
                    DaysOffset += a-CurrentDayOfWeek;
                    if(DaysOffset < 0) DaysOffset += 7;
                    if(DaysOffset == 0 && SetTime < CurrentTime)
                    {
                        DaysOffset = 7;
                        CheckNext=true; //Jesli znajdzie wczesniejsza date to korzysta z tego ustawionego 7
                    }
                    QDate Res = ResultDat.addDays(DaysOffset);
                    (*AlarmsTabPointer)[x]->SetOnceDate(Res.day(), Res.month(),Res.year());
                    if(!CheckNext) break;
                }
                if(a == 6 && !(IsFromBeggining))
                {
                    a=-1;
                    IsFromBeggining=true;
                }
                DaysOffset=0;
                CheckNext=false;
            }
        }
        else if((*AlarmsTabPointer)[x]->GetEnabledStatus() && (*AlarmsTabPointer)[x]->GetOnceStatus())
        {
            QDateTime CurrentDateTime = QDateTime::currentDateTime();
            QDateTime OnceDateTime;
            OnceDateTime.setDate((*AlarmsTabPointer)[x]->GetOnceDate());
            OnceDateTime.setTime((*AlarmsTabPointer)[x]->GetTime());

            if(OnceDateTime < CurrentDateTime)
            {
                (*AlarmsTabPointer)[x]->SetEnabled(false);
                RefreshButtons();
            }
            /*   if(CurDat == (*AlarmsTabPointer)[x]->GetOnceDate() && SetTime < CurrentTime)
            {
                QDate Res = ResultDat.addDays(7);
                (*AlarmsTabPointer)[x]->SetOnceDate(Res.day(), Res.month(),Res.year());
            } */
        }
        DaysOffset=0;
        qDebug((*AlarmsTabPointer)[x]->GetOnceDate().toString("dd.MM.yyyy").toUtf8());
    }
}

void AlarmsList::ConnectToAlbumWindow(AlbumsLibrary *point)
{
    connect(point, SIGNAL(SendTrackPath(QString)), &AlarmSetter, SLOT(GetAlrValue(QString)));
    connect(&AlarmSetter, SIGNAL(OpenSetWindow(int)), point, SLOT(OpenInSelectAlarmMode(int)));
}
