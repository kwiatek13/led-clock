#ifndef ALBUMSLIBRARY_H
#define ALBUMSLIBRARY_H

#include <QDialog>
#include "player.h"
#include "playlist.h"
#include "playerwindow.h"
#include <QPixmap>
#include "customalbumrow.h"
#include "popupdialog.h"
#include "alarmset.h" //Dla dostepu do enuma z trybami wyboru dzwieku alarmu
#include <QThread>

static const int MaxRows=4;
static const int MaxColumns=6;

namespace Ui {
class AlbumsLibrary;
}

class AlbumsLibrary : public QDialog
{
    Q_OBJECT

public:
    explicit AlbumsLibrary(QWidget *parent = 0);
    ~AlbumsLibrary();
    void SetPointers(playlist *PP, player *PlP, PlayerWindow *Plw);
    void ListFolders();
    void LoadAlbumsLibrary(int Page);
    void ClearButtonsTab();

public slots:
    void OpenInSelectAlarmMode(int CurrMode);

private slots:
    void on_BackButton_clicked();

    void on_NextPage_clicked();

    void on_PrevPage_clicked();

    void on_Tracklist_clicked(const QModelIndex &index);

    void on_PlayerButton_clicked();

private:
    Ui::AlbumsLibrary *ui;
    playlist *PlaylistPointer = nullptr;
    player *PlayerPointer = nullptr;
    PlayerWindow *PlayerWindowPointer = nullptr;
    void AddButton(QString filepath);
    QPixmap setPixmapImg(QString sciezka, QString plik);
    void setPixmapImg(QImage Image);
    void SetAlbumPage(QString sciezka);
    QPixmap image;
    QStringList image_files;
    QDir folder;
    QVector <QPushButton*> button_tab;
    QList<QMediaContent> content;
    std::shared_ptr<QMediaPlaylist> lista = nullptr;
    QStringList files;
    QFileInfo fi;
    PopupDialog Popup;
    QStringList PlList;
    int x=0, y=0;
    int CurPage=0;
    int SelectedTrack=0;
    //pos=0;
    bool IsAlbumPageOpened=false;
    bool SelectAlarmMode=false;

    QString FolderPath="/home/pi/MusicLibrary";
    QString OtherTracksPath="/home/pi/OtherTracks";

private slots:
    void GetPlalylsistName(QString Text);
    void PreparePopupDialog(int TrackNumber);
    void AddToSelectedPlaylist(QString PlaylistName);
    void AddToFavourites(int TrackNumber);
    void ReceiveFullRadioEntry(QString Index);

signals:
    void SendTrackPath(QString Path);


};

#endif // ALBUMSLIBRARY_H
