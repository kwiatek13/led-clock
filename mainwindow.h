#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QDateTime>
#include "circle.h"
#include "alarmsetdialog.h"
#include "alarmset.h"
#include "alarmslist.h"
#include "alarmwindow.h"
#include "multimediaplayerwindow.h"
#include "QMediaPlaylist"
#include "ledcontrol.h"
#include "lightsettings.h"
#include "gpiodevicesread.h"
#include <QProcess>
#include <QFile>

constexpr int SnoozeTime=5*60;
constexpr int TimeoutAlarm=20*60*1000;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void LaunchAlarm();

private slots:
    void TimeRefresh();

    void on_pushButton_clicked();

    void SetCloseAlarm();

    void ShowAlarmRemaining();

    void SnoozeSlot();

    void OffAlarmSlot();

    void on_MultimediaButton_clicked();

    void CheckRadioPlayback();

    void on_LampSettingsButton_clicked();

    void RefreshDHTRead();

    void DHTRead();

    void ReceivedRCCode(unsigned long CurrentCode);

    void ReceivedRCCombination(int CurrentCombination);

private:
    Ui::MainWindow *ui;
   CPBar Circle;
   QTimer TimeChange;
   QTimer AlarmRemainingTimer;
   QTimer TimeoutTimer;
   QDateTime Times;
   QDateTime SunriseDate;
   bool CheckSunrise = false;
   AlarmSetDialog AlarmSetter;
   AlarmSet SnoozeAlarm;
   AlarmSet *ClosestAlarm;
   AlarmsList AlarmsDialog;
   bool AlarmActivated=false;
   AlarmWindow WakeupWindow;
   MultimediaPlayerWindow MultimediaWindow;
   LedControl LightControl;
   LightSettings LightSettingsWindow;
   int FailCounter=0;
   int LaunchLampCounter=0;
   DeviceData SharedDeviceData;
   GPIODevicesRead *DevicesRead;
   QThread DevicesReadThread;
   QFile TempOutputFile;


   QProcess DHTProcess;

   QVector <AlarmSet*> AlarmsTab;
   QString DefAlarmPath = "/home/pi/DefAlarm.mp3";
};

#endif // MAINWINDOW_H
