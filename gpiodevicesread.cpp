#include "gpiodevicesread.h"

GPIODevicesRead::GPIODevicesRead(DeviceData *Pointer)
{
    SharedPointer = Pointer;

    pinMode(ButtonPin, INPUT);


    RadioSwitch = RCSwitch();
    if (pulseLength != 0) RadioSwitch.setPulseLength(pulseLength);
    RadioSwitch.setReceiveTolerance(75);
    RadioSwitch.enableReceive(ReceiverPin);  // Receiver on interrupt 0 => that is pin #2
}

void GPIODevicesRead::SnoozeButtonRead()
{
    if(digitalRead(ButtonPin) == HIGH)
        SharedPointer->IsSnoozeButtonPushed = true;
    else
        SharedPointer->IsSnoozeButtonPushed = false;

    //qDebug () <<"State: " <<SharedPointer->IsSnoozeButtonPushed <<"\n";
}

void GPIODevicesRead::ReadProcess()
{
    //int DHTCounter = 1;
    while(true)
    {
        SnoozeButtonRead();
        ReadReceiverData();
//        if(DHTCounter % 101 == 0)
//        {
//            //read_dht_data();
//            DHTCounter = 1;
//        }
//        else
//            DHTCounter++;
        QThread::msleep(20);
    }
}

void GPIODevicesRead::read_dht_data()
{
    uint8_t laststate	= HIGH;
    uint8_t counter		= 0;
    uint8_t j			= 0, i;

    DHTData[0] = DHTData[1] = DHTData[2] = DHTData[3] = DHTData[4] = 0;

    /* pull pin down for 18 milliseconds */
    pinMode( DHTPin, OUTPUT );
    digitalWrite( DHTPin, LOW );
    delay( 18 );

    /* prepare to read the pin */
    pinMode( DHTPin, INPUT );

    /* detect change and read data */
    for ( i = 0; i < 85; i++ )
    {
        counter = 0;
        while ( digitalRead( DHTPin ) == laststate )
        {
            counter++;
            delayMicroseconds( 1 );
            if ( counter == 255 )
            {
                break;
            }
        }
        laststate = digitalRead( DHTPin );

        if ( counter == 255 )
            break;

        /* ignore first 3 transitions */
        if ( (i >= 4) && (i % 2 == 0) )
        {
            /* shove each bit into the storage bytes */
            DHTData[j / 8] <<= 1;
            if ( counter > 16 )
                DHTData[j / 8] |= 1;
            j++;
        }
    }

    /*
     * check we read 40 bits (8bit x 5 ) + verify checksum in the last byte
     * print it out if data is good
     */
    //printf("j=%d \n", j);
    //qDebug () <<"j= " <<j <<"\n";
    if ( (j >= 40) &&
         (DHTData[4] == ( (DHTData[0] + DHTData[1] + DHTData[2] + DHTData[3]) & 0xFF) ) )
    {
        printf("Validation OK! \n");
        float h = (float)((DHTData[0] << 8) + DHTData[1]) / 10;
        if ( h > 100 )
        {
            h = DHTData[0];	// for DHT11
        }
        float c = (float)(((DHTData[2] & 0x7F) << 8) + DHTData[3]) / 10;
        if ( c > 125 )
        {
            c = DHTData[2];	// for DHT11
        }
        if ( DHTData[2] & 0x80 )
        {
            c = -c;
        }
        float f = c * 1.8f + 32;
        //printf( "Humidity = %.1f %% Temperature = %.1f *C (%.1f *F)\n", h, c, f );
        qDebug () <<"Humidity: " <<h <<" Temperature: " <<c <<"\n";
    }else  {
        qDebug () << "Data not good, skip\n" ;
    }
}

void GPIODevicesRead::ReadReceiverData()
{

          if (RadioSwitch.available()) {

              qDebug () <<"Is available! \n";
            int value = RadioSwitch.getReceivedValue();

            if (value == 0) {
              qDebug () <<"Unknown encoding\n";
            } else {
            // qDebug () <<"Received: " <<RadioSwitch.getReceivedValue() <<"\n";
             SharedPointer->RCCode = RadioSwitch.getReceivedValue();
             if(LastRCState != SharedPointer->RCCode)
             {
                 Combination.push_back(SharedPointer->RCCode);
                 CombinationTime = std::chrono::system_clock::now();
                 emit RCButtonPressed(SharedPointer->RCCode);
             }
             ResetCounter = 25;
             CheckCombination();
            }

            fflush(stdout);
            RadioSwitch.resetAvailable();
          }
          else if(SharedPointer->RCCode != 0 && ResetCounter <= 0) SharedPointer->RCCode = 0;
          else if(ResetCounter > 0) ResetCounter--;

          if(Combination.size() > 0)
          {
              auto CurrentTime = std::chrono::system_clock::now();;
              if(std::chrono::duration_cast<std::chrono::milliseconds>(CurrentTime - CombinationTime).count() > 2000) Combination.clear();
          }

          LastRCState = SharedPointer->RCCode;
}

void GPIODevicesRead::CheckCombination()
{
    if(Combination.size() >= 3)
    {
        if(Combination[0] == ButtonA)
        {
            if(Combination[1] ==  ButtonB && Combination[2] == ButtonD) SharedPointer->TypedCombination = 1;
            emit CombinationTyped(1);
        }

        else if(Combination[0] == ButtonB)
        {
            if(Combination[1] == ButtonA && Combination[2] == ButtonC) SharedPointer->TypedCombination = 2;
            emit CombinationTyped(2);
        }
    }

}
