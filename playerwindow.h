#ifndef PLAYERWINDOW_H
#define PLAYERWINDOW_H

#include <QDialog>
#include <QMediaPlayer>
#include "player.h"
#include <QTimer>
#include <random>

namespace Ui {
class PlayerWindow;
}

class PlayerWindow : public QDialog
{
    Q_OBJECT

public:
    explicit PlayerWindow(QWidget *parent = 0);
    ~PlayerWindow();
    void SetPlayer(player* point);
    void SetTrackList(QStringList DirList);
    void AddToTrackList(QString Title);
    void SetCoverImage(QString Path);
    void SetCoverImage(QImage Image);
    void DeleteFromList(int Position);
    void SetRadioMode(QString RadioStationName);

private slots:
    void on_ListButton_clicked();

    void on_PreviousButton_clicked();

    void on_NextButton_clicked();

    void on_PlayButton_clicked();

    void on_horizontalSlider_sliderMoved(int position);

    void on_horizontalSlider_sliderPressed();

    void on_horizontalSlider_sliderReleased();

    void on_BackButton_clicked();

private:
    Ui::PlayerWindow *ui;
    player *PlayerPointer;
    int Volume = 10;
    QTimer VolTimer;
    bool IsList=false;
    int mode=0;
    QFileInfo fi;
    QPixmap Pixmap;
    bool IsShuffle=false;
    bool IsNormalPlayback=false;
    bool IsRadioMode=false;
    int NumberOfTracks=0;
    QVector<int> IndexSet;
    QString NullTrack="/home/pi/NullSound.mp3";
    void PrepareIndexSet();

private slots:
    void OnDurationChanged(qint64 position);
    void OnPositionChanged(qint64 position);
    void on_CurrentTracklist_clicked(const QModelIndex &index);
    void OnPlaybackChanged(bool Val);
    void on_ShuffleButton_clicked();
    void ClearLabel();
    void on_VolumeDownButton_clicked();
    void on_VolumeUpButon_clicked();
};

#endif // PLAYERWINDOW_H
