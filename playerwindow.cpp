#include "playerwindow.h"
#include "ui_playerwindow.h"

PlayerWindow::PlayerWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PlayerWindow)
{
    ui->setupUi(this);
    VolTimer.setSingleShot(true);
    connect(&VolTimer, SIGNAL(timeout()), this, SLOT(ClearLabel()));
}

PlayerWindow::~PlayerWindow()
{
    delete ui;
}

void PlayerWindow::SetPlayer(player *point)
{
    PlayerPointer = point;
    connect(PlayerPointer->getPlayer(),SIGNAL(positionChanged(qint64)), this, SLOT(OnPositionChanged(qint64)));
    connect(PlayerPointer->getPlayer(), SIGNAL(durationChanged(qint64)), this, SLOT(OnDurationChanged(qint64)));
    connect(PlayerPointer, SIGNAL(PlaybackStateChanged(bool)), this, SLOT(OnPlaybackChanged(bool)));
    PlayerPointer->getPlayer()->setVolume(Volume);
}

void PlayerWindow::SetTrackList(QStringList DirList)
{
    //Przy ladowaniu stacji radiowej nie wywolujemy SetTrackList
    if(IsRadioMode)
    {
        ui->ListButton->setVisible(true);
        ui->NextButton->setVisible(true);
        ui->PreviousButton->setVisible(true);
        ui->horizontalSlider->setVisible(true);
        ui->ShuffleButton->setVisible(true);
        ui->TimeLabel->setVisible(true);
        connect(PlayerPointer->getPlayer(),SIGNAL(positionChanged(qint64)), this, SLOT(OnPositionChanged(qint64)));
        connect(PlayerPointer->getPlayer(), SIGNAL(durationChanged(qint64)), this, SLOT(OnDurationChanged(qint64)));
        IsRadioMode = false;
    }
    ui->CurrentTracklist->clear();
    NumberOfTracks=0;
    for(const QString& f:DirList)
    {
        //content.push_back(QUrl::fromLocalFile(Directory+"/" + f));
     fi.setFile(f);

         ui->CurrentTracklist->addItem(fi.completeBaseName());
         NumberOfTracks++;

    }
    if(IsShuffle)
    {
        IndexSet.clear();
        PrepareIndexSet();
        PlayerPointer->getPlayer()->playlist()->addMedia(QMediaContent(QUrl::fromLocalFile(NullTrack)));
        IsNormalPlayback=true;
    }
}

void PlayerWindow::AddToTrackList(QString Title)
{
    if(IsShuffle)
    {
        QMediaPlaylist *TempPoint = PlayerPointer->getPlayer()->playlist();
        QMediaContent TempContent = TempPoint->media(NumberOfTracks+1);
        TempPoint->insertMedia(NumberOfTracks, TempContent);
        TempPoint->insertMedia(NumberOfTracks+1, QMediaContent(QUrl::fromLocalFile(NullTrack)));

        IndexSet.push_back(NumberOfTracks);
    }


    ui->CurrentTracklist->addItem(Title);

    NumberOfTracks++;

}

void PlayerWindow::SetCoverImage(QString Path)
{
    //fprintf(stderr, "Path: %s", Path.toLatin1());
    Pixmap.load(Path);
    Pixmap = Pixmap.scaledToWidth(ui->CoverLabel->width(), Qt::SmoothTransformation);
    Pixmap = Pixmap.scaledToHeight(ui->CoverLabel->height(), Qt::SmoothTransformation);
    ui->CoverLabel->setPixmap(Pixmap);
}

void PlayerWindow::SetCoverImage(QImage Image)
{
    Pixmap = QPixmap::fromImage(Image);
    Pixmap = Pixmap.scaledToWidth(ui->CoverLabel->width(), Qt::SmoothTransformation);
    Pixmap = Pixmap.scaledToHeight(ui->CoverLabel->height(), Qt::SmoothTransformation);
    ui->CoverLabel->setPixmap(Pixmap);
}

void PlayerWindow::on_ListButton_clicked()
{
    if(!IsList)
    {
        ui->stackedWidget->setCurrentIndex(1);
        IsList=true;
        ui->ListButton->setText("Odtwarzacz");
    }
    else
    {
        ui->stackedWidget->setCurrentIndex(0);
        IsList=false;
        ui->ListButton->setText("Lista");
    }
}

void PlayerWindow::on_PreviousButton_clicked()
{
    PlayerPointer->PreviousTrack();
}

void PlayerWindow::on_NextButton_clicked()
{
    PlayerPointer->NextTrack();
}

void PlayerWindow::on_PlayButton_clicked()
{
    PlayerPointer->odtworz(mode);
}

void PlayerWindow::on_horizontalSlider_sliderMoved(int position)
{
    PlayerPointer->getPlayer()->setPosition(position);
}

void PlayerWindow::on_horizontalSlider_sliderPressed()
{
    PlayerPointer->odtworz(2);
}

void PlayerWindow::on_horizontalSlider_sliderReleased()
{
     PlayerPointer->odtworz(0);
}

void PlayerWindow::on_BackButton_clicked()
{
    close();
}

void PlayerWindow::PrepareIndexSet()
{
    QMediaPlayer *TempPoint = PlayerPointer->getPlayer();
    QVector <int> TempVector;
    int TempVal;
    thread_local std::mt19937 gen{std::random_device{}()};
    for(int x=0; x < NumberOfTracks;x++)
    {

       if(x != TempPoint->playlist()->currentIndex()) TempVector.push_back(x);
    }

    while(TempVector.size() > 0)
    {
        TempVal = std::uniform_int_distribution<int>{0, TempVector.size()-1}(gen);
        IndexSet.push_back(TempVector[TempVal]);
        TempVector.remove(TempVal);
    }

}

void PlayerWindow::OnDurationChanged(qint64 position)
{
    if(IsShuffle)
    {
        if(!IsNormalPlayback)
        {
            if(IndexSet.size() <= 0)
            {
                PlayerPointer->odtworz(3);
                PrepareIndexSet();
                return;
            }
            thread_local std::mt19937 gen{std::random_device{}()};
            int RandomVal = std::uniform_int_distribution<int>{0, IndexSet.size()-1}(gen);

            PlayerPointer->getPlayer()->playlist()->setCurrentIndex(IndexSet[RandomVal]);
            PlayerPointer->odtworz(0);
            IndexSet.remove(RandomVal);
            IsNormalPlayback = true;
            return;
        }
        else
        {
            IsNormalPlayback = false;
        }
    }


    ui->horizontalSlider->setMaximum(position);
    if(NumberOfTracks <= 0) return;
    ui->TitleLabel->setText(QString(PlayerPointer->getPlayer()->metaData("Title").toString()));
    ui->CurrentTracklist->setCurrentRow(PlayerPointer->getPlayer()->playlist()->currentIndex());
   // qDebug () <<"Media count: " << PlayerPointer->getPlayer()->playlist()->mediaCount() <<"\n";
    QString TempPath = PlayerPointer->getPlayer()->playlist()->currentMedia().canonicalUrl().toString();
    TempPath.remove(QString("file://"));
    TempPath.remove(QString(".mp3"));
    TempPath.remove(QString(".flac"));
    TempPath.remove(QString(".wav"));
    TempPath.remove(ui->CurrentTracklist->currentItem()->text());
    QDir folder;
    folder.setPath(TempPath);

    QImage TempImage = PlayerPointer->getPlayer()->metaData("CoverArtImage").value<QImage>();
    if(TempImage.isNull())
    {
        QStringList image_files = folder.entryList(QStringList() << "*.jpg" <<"*.png",QDir::Files);
        if(!image_files.empty())
            SetCoverImage(QString(TempPath+image_files.first()));
        else
            SetCoverImage("/home/pi/albumart.jpg");
    }
    else
    {
        SetCoverImage(TempImage);
    }
  //  qDebug () << "Path: " << TempPath <<"\n";
}

void PlayerWindow::OnPositionChanged(qint64 position)
{
    ui->horizontalSlider->setValue(position);

        QString SSeconds;
        int Seconds = position/1000;
        int Minutes = Seconds/60;
        Seconds = Seconds - Minutes*60;

        if(Seconds < 10) SSeconds = "0";

        SSeconds += QString::number(Seconds);

        ui->TimeLabel->setText(QString("%1:%2").arg(QString::number(Minutes)).arg(SSeconds));

}

void PlayerWindow::on_CurrentTracklist_clicked(const QModelIndex &index)
{
    if(IsShuffle)
    {
        IsNormalPlayback=true;
        for(int z=0; z < IndexSet.size(); z++)
        {
            if(IndexSet[z] == ui->CurrentTracklist->currentRow())
            {
                IndexSet.remove(z);
                break;
            }
        }
    }
    PlayerPointer->getPlayer()->playlist()->setCurrentIndex(ui->CurrentTracklist->currentRow());
    PlayerPointer->odtworz(0);
    //ui->PlayButton->setText("||");
}

void PlayerWindow::OnPlaybackChanged(bool Val)
{
    if(Val)
    {
        mode=1;
        ui->PlayButton->setIcon(QIcon(":/Graphics/PauseIcon.png"));
    }
    else
    {
        mode=0;
        ui->PlayButton->setIcon(QIcon(":/Graphics/PlayIcon.png"));
    }
}

void PlayerWindow::on_ShuffleButton_clicked()
{
    if(IsShuffle)
    {
        ui->ShuffleButton->setStyleSheet("background-color: white;");
        IsShuffle = false;
        IndexSet.clear();
        PlayerPointer->getPlayer()->playlist()->removeMedia(NumberOfTracks);
    }
    else
    {
        ui->ShuffleButton->setStyleSheet("background-color: green;");
        IsShuffle = true;
        PlayerPointer->getPlayer()->playlist()->addMedia(QMediaContent(QUrl::fromLocalFile(NullTrack)));
        PrepareIndexSet();
    }
}

void PlayerWindow::ClearLabel()
{
    ui->VolumeLabel->setText("");
}

void PlayerWindow::on_VolumeDownButton_clicked()
{
    if(Volume <= 0) return;
    VolTimer.stop();
    int VolVal;
    Volume -= 2;
    PlayerPointer->getPlayer()->setVolume(Volume);
    VolVal = (Volume*100)/MaxVolume;
    ui->VolumeLabel->setText(QString::number(VolVal)+"%");

    VolTimer.start(5000);
}

void PlayerWindow::on_VolumeUpButon_clicked()
{
    if(Volume >= MaxVolume) return;
    VolTimer.stop();
    int VolVal;
    Volume += 2;
    PlayerPointer->getPlayer()->setVolume(Volume);
    VolVal = (Volume*100)/MaxVolume;
    ui->VolumeLabel->setText(QString::number(VolVal)+"%");

    VolTimer.start(5000);

}

void PlayerWindow::DeleteFromList(int Position)
{
    ui->CurrentTracklist->model()->removeRow(Position);
}

void PlayerWindow::SetRadioMode(QString RadioStationName)
{
    ui->TitleLabel->setText(RadioStationName);
    if(!IsRadioMode)
    {
        IsRadioMode = true;
        ui->ListButton->setVisible(false);
        ui->NextButton->setVisible(false);
        ui->PreviousButton->setVisible(false);
        ui->horizontalSlider->setVisible(false);
        ui->TimeLabel->setVisible(false);
        ui->ShuffleButton->setVisible(false);
        disconnect(PlayerPointer->getPlayer(),SIGNAL(positionChanged(qint64)), this, SLOT(OnPositionChanged(qint64)));
        disconnect(PlayerPointer->getPlayer(), SIGNAL(durationChanged(qint64)), this, SLOT(OnDurationChanged(qint64)));
        SetCoverImage(":/Graphics/RadioIcon.png");
    }
}
