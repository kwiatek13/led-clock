#ifndef FILESMANAGER_H
#define FILESMANAGER_H

#include <QDialog>
#include <QFile>
#include <QTextStream>
#include <QTimer>
#include <QProcess>
#include <QDir>
#include "playlist.h"
#include "customalbumrow.h"
#include <QListWidgetItem>
#include <QThread>
#include <QMutex>
#include <QMessageBox>

static const int USBRefreshTime = 3*1000;
static const int MaxLibrarySize = 15000;

enum Mode : int
{
    CpFiles=1,
    CpDirectories,
    DelFiles,
    DelDirectories
};

namespace Ui {
class FilesManager;
}

class ManagerEngine : public QObject
{
    Q_OBJECT
public:
    ManagerEngine();
    void DeleteFiles(QStringList *PathList);
    void DeleteDirectories(QStringList *DirList);
    void CopyDirectories(QStringList *DirsList);
    void CopyFiles(QStringList *PathList);
    void SetPointer(playlist *point, QStringList *Files, QStringList *Dirs, int *Mode, QMutex *MutexP);
    double GetDirectorySize(QString FilePath);
   // void Copy

private:
    QFile TargetFile;
    QFile USBFile;
    playlist *PlaylistPointer;
    QStringList PlaylistsList;
    QStringList *FilesLisPtr;
    QStringList *DirsListPtr;
    unsigned int LibrarySize=0;
    int *ModePtr;
    int LocalCounter=0;
    void StartDeleteProcedure(QStringList *PathsList, QStringList *DirList);
    void StartCopyProcedure(QStringList *PathsList, QStringList *DirList);
    QMutex *MutexPtr;

public slots:
    void StartProcess();

signals:
    void SendCurrentStatus(QString status);
    void SendCurrentLibrarySize(unsigned int CurrentSize);
    void Finish();
};

class FilesManager : public QDialog
{
    Q_OBJECT

public:
    explicit FilesManager(QWidget *parent = 0);
    ~FilesManager();
    void ListUSBDriveFiles(QString Path, bool BackDisabled=false);
    void ListUSBDriveDirectories(QString Path, bool BackDisabled=false);
    void SetPointer(playlist *pl);

private:
    Ui::FilesManager *ui;
    QStringList FoldersToDeleteList;
    QStringList FilesToDeleteList;
    QVector <bool> SelectStates;
    int ListOffset=0;
    QVector <QString> LastPath;
    QString CurrentPath;
    QStringList FilesToExecute;
    QStringList DirsToExecute;
    int SharedMode=0;
    bool IsFilesMode=true;
    bool USBLastState = false;
    QFile USBFile;
    QProcess DevProcess;
    bool IsAlreadyMounted=false;
    QStringList CurrentFilesList;
    ManagerEngine Engine;
    QThread EngineThread;
    QMutex SharedMutex;
    QMessageBox FinishBox;


    private slots:
    void USBScan();
    void SetSelectFlag(int index);
    void JumpToSubFolder(QListWidgetItem *item);
    void on_BackButton_clicked();
    void on_AddTracksButton_clicked();
    void on_AddAlbumsButton_clicked();

    void on_ExecuteButton_clicked();

    void on_DelAlbumsButton_clicked();

    void on_DelTracksButton_clicked();
    void ProcFinished();
    void GetCurrentStatus(QString Status);
    void SetLibrarySizeIndicator(unsigned int CurrentSize);

signals:
    void USBAvailability(bool State);
    void RefreshLibrary();
};



#endif // FILESMANAGER_H
