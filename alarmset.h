#ifndef ALARMSET_H
#define ALARMSET_H

#include <QObject>
#include <QDate>
#include <QTime>
#include <QDateTime>

enum AlrMode : int
{
    DefaultSignal = 0,
    FunStart,
    SelectedTrackMod,
    Radio
};

class AlarmSet : public QObject
{
    Q_OBJECT
public:
    explicit AlarmSet(QObject *parent = nullptr);
    void SetEnabled(bool Val);
    void SetAsOnce(bool Val);
    void SetDays(int Day, bool Val);
    void SetHour(int Hour, int minute);
    void SetOnceDate(int Day, int Month, int Year);
    void SetSecs(int Val);
    void SetAlarmMode(int Val);
    void SetAlarmValue(QString Val);
    void SetSunriseEnabled(bool Val);
    void SetSunriseTime(int Val);

    bool GetEnabledStatus();
    bool GetOnceStatus();
    bool GetDayState(int Day);
    QTime GetTime();
    QDate GetOnceDate();

    int GetAlarmMode();
    QString GetAlarmValue();

    bool GetIsSunriseEnabled();
    int GetSunriseTime();


private:
    bool IsEnabled=false;
    bool IsOnce=false;
    QDate Date;
    QTime Time;
    bool Days[7];

    int AlarmMode = DefaultSignal;
    QString AlarmValue;

    bool IsSunriseEnabled = true;
    int SunriseTime = 3600;



signals:

public slots:
};

#endif // ALARMSET_H
