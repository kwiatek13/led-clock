#include "popupdialog.h"
#include "ui_popupdialog.h"

PopupDialog::PopupDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PopupDialog)
{
    ui->setupUi(this);
    setWindowFlags(Qt::FramelessWindowHint);
}

PopupDialog::~PopupDialog()
{
    delete ui;
}

void PopupDialog::SetAsAcceptWindow(QString Tx)
{
    ui->EnterEdit->setVisible(false);
    ui->List->setVisible(false);
    ui->EnterEdit2->setVisible(false);
    ui->TextLabel2->setVisible(false);
    ui->TextLabel->setText(Tx);
    RetVal = "1";
}

void PopupDialog::SetAsAcceptWindowWithReturn(QString Tx, QString ReturnValue)
{
    ui->EnterEdit->setVisible(false);
    ui->List->setVisible(false);
    ui->EnterEdit2->setVisible(false);
    ui->TextLabel2->setVisible(false);
    ui->TextLabel->setText(Tx);
    RetVal = ReturnValue;
}

void PopupDialog::SetAsSelectOptionWindow(QString Tx, QStringList List, bool IndexReturn)
{
    ui->List->clear();
    ui->TextLabel->setText(Tx);
    ui->EnterEdit->setVisible(false);
    ui->OkButton->setVisible(false);
    ui->EnterEdit2->setVisible(false);
    ui->TextLabel2->setVisible(false);
    for(int x=0; x < List.length(); x++)
    {
        ui->List->addItem(List[x]);
    }
    IsIndexReturn = IndexReturn;
}

void PopupDialog::SetAsEnterWindow(QString Tx)
{
    ui->List->setVisible(false);
    ui->EnterEdit2->setVisible(false);
    ui->TextLabel2->setVisible(false);
    ui->TextLabel->setText(Tx);
}

void PopupDialog::SetAsEnterWindowWith2Args(QString Tx, QString Tx2)
{
    ui->List->setVisible(false);
    ui->TextLabel->setText(Tx);
    ui->TextLabel2->setText(Tx2);
}

void PopupDialog::on_OkButton_clicked()
{
    emit ReturnValue(RetVal);
    close();
    ui->EnterEdit->clear();
    ui->EnterEdit2->clear();
}



void PopupDialog::on_EnterEdit_editingFinished()
{
    RetVal = ui->EnterEdit->text();
    if(ui->EnterEdit2->text() != "") RetVal = ui->EnterEdit->text()+";"+ui->EnterEdit2->text();
}

void PopupDialog::on_List_clicked(const QModelIndex &index)
{
    if(!IsIndexReturn)
    {
        RetVal = ui->List->currentItem()->text();
        if(RetVal != "playlist") RetVal += ".pla";
    }
    else RetVal = QString::number(ui->List->currentRow());
    ui->OkButton->setVisible(true);
}

void PopupDialog::on_CancelButton_clicked()
{
    close();
    ui->EnterEdit->clear();
    ui->EnterEdit2->clear();
}

void PopupDialog::on_EnterEdit2_editingFinished()
{
    RetVal = ui->EnterEdit->text()+";"+ui->EnterEdit2->text();
}
