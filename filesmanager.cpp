#include "filesmanager.h"
#include "ui_filesmanager.h"

FilesManager::FilesManager(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FilesManager)
{
    ui->setupUi(this);
    USBFile.setFileName("/dev/sda1");
    ui->LibrarySizeBar->setRange(0,100);
    FinishBox.setText("Kopiowanie zakonczone!");

    USBScan();
}

FilesManager::~FilesManager()
{
    delete ui;
}

void FilesManager::ListUSBDriveFiles(QString Path, bool BackDisabled)
{
    ui->DataList->clear();
    SelectStates.clear();
    CurrentFilesList.clear();
    //CurrentPath = Path;
    //LastPath.push_back(Path);
    ListOffset=0;
    QDir TempDir(Path);
    CurrentPath = TempDir.absolutePath();
    auto Flags = QDir::Dirs|QDir::NoDot;
    if(TempDir.absolutePath() == "/mnt"|| BackDisabled) Flags = QDir::Dirs|QDir::NoDotAndDotDot;
    //QStringList TempList =
    CurrentFilesList = TempDir.entryList(Flags);
    QFileInfo TempInfo;
    QDir TempDir2;

   /* if(!PrevPath.isEmpty())
    {
        ui->DataList->addItem("..");
    } */

    for(int a=0; a < CurrentFilesList.size(); a++)
    {
        TempDir2.setPath(CurrentFilesList[a]);
        ui->DataList->addItem(TempDir2.dirName());
        ui->DataList->item(a)->setSizeHint(QSize(ui->DataList->item(a)->sizeHint().height(), 40));
        SelectStates.push_back(false);

    }
    ListOffset = CurrentFilesList.size();

    CurrentFilesList += TempDir.entryList(QStringList() << "*.flac" << "*.mp3", QDir::Files);

    for(int x=ListOffset; x < CurrentFilesList.size(); x++)
    {
        TempInfo.setFile(CurrentFilesList[x]);
        CustomAlbumRow *TempPoint = new CustomAlbumRow(this, FileManagerMode);
        QListWidgetItem *TempPointItem = new QListWidgetItem;

        connect(TempPoint->CheckBoxPtr,&QCheckBox::clicked,[=](){SetSelectFlag(x);}); //przypisywanie funkcji do przyciskow
      //  connect(TempPoint->FavouriteButton,&QPushButton::clicked,[=](){AddToFavourites(NumberOfItems);}); //przypisywanie funkcji do przyciskow
        TempPointItem->setSizeHint(QSize(TempPointItem->sizeHint().height(), 40));
        TempPoint->SetLabel(TempInfo.completeBaseName());
           ui->DataList->addItem(TempPointItem);
           ui->DataList->setItemWidget(TempPointItem, TempPoint);
           SelectStates.push_back(false);
      //  NumberOfItems++;

    }



}

void FilesManager::ListUSBDriveDirectories(QString Path, bool BackDisabled)
{
    ui->DataList->clear();
    SelectStates.clear();
    CurrentFilesList.clear();
    //CurrentPath = Path;
    //LastPath.push_back(Path);
    ListOffset=0;
    QDir TempDir(Path);
    CurrentPath = TempDir.absolutePath();
    auto Flags = QDir::Dirs|QDir::NoDot;
    if(TempDir.absolutePath() == "/mnt" || BackDisabled) Flags = QDir::Dirs|QDir::NoDotAndDotDot;
    //QStringList TempList =
    CurrentFilesList = TempDir.entryList(Flags);
    QFileInfo TempInfo;
    QDir TempDir2;

    /* if(!PrevPath.isEmpty())
    {
        ui->DataList->addItem("..");
    } */

    for(int a=0; a < CurrentFilesList.size(); a++)
    {
        TempDir2.setPath(CurrentFilesList[a]);

        if(TempDir2.dirName() == "..")
        {
            ui->DataList->addItem(TempDir2.dirName());
            ui->DataList->item(a)->setSizeHint(QSize(ui->DataList->item(a)->sizeHint().height(), 40));
        }
        else
        {
            CustomAlbumRow *TempPoint = new CustomAlbumRow(this, FileManagerMode);
            QListWidgetItem *TempPointItem = new QListWidgetItem;

            connect(TempPoint->CheckBoxPtr,&QCheckBox::clicked,[=](){SetSelectFlag(a);}); //przypisywanie funkcji do przyciskow
            //  connect(TempPoint->FavouriteButton,&QPushButton::clicked,[=](){AddToFavourites(NumberOfItems);}); //przypisywanie funkcji do przyciskow
            TempPointItem->setSizeHint(QSize(TempPointItem->sizeHint().height(), 40));
            //TempPointItem->setText(TempDir2.dirName());
            TempPoint->SetLabel(TempDir2.dirName());
           // TempPointItem->setHidden(true);
            ui->DataList->addItem(TempPointItem);
            ui->DataList->setItemWidget(TempPointItem, TempPoint);           
        }
        SelectStates.push_back(false);

    }
    ListOffset = CurrentFilesList.size();
}

void FilesManager::SetPointer(playlist *pl)
{
    connect(&Engine, SIGNAL(SendCurrentLibrarySize(unsigned int)), this, SLOT(SetLibrarySizeIndicator(unsigned int)));
    Engine.SetPointer(pl, &FilesToExecute, &DirsToExecute, &SharedMode, &SharedMutex);
    Engine.moveToThread(&EngineThread);
    connect(&EngineThread, SIGNAL(started()), &Engine, SLOT(StartProcess()));
    connect(&EngineThread, SIGNAL(finished()), this, SLOT(ProcFinished()));
    connect(&Engine, SIGNAL(SendCurrentStatus(QString)), this, SLOT(GetCurrentStatus(QString)));
    connect(&Engine, SIGNAL(Finish()), &EngineThread, SLOT(quit()));
}

//Znalazlem w internecie ta metode i wyglada calkiem dobrze moim zdaniem
double ManagerEngine::GetDirectorySize(QString FilePath)
{
    double size = 0;
    QDir dir(FilePath);

    QDir::Filters fileFilters = QDir::Files|QDir::System|QDir::Hidden;
    for(QString filePath : dir.entryList(fileFilters)) {
        QFileInfo fi(dir, filePath);
        size+= fi.size();
    }

    QDir::Filters dirFilters = QDir::Dirs|QDir::NoDotAndDotDot|QDir::System|QDir::Hidden;
    for(QString childDirPath : dir.entryList(dirFilters))
        size+= GetDirectorySize(FilePath + QDir::separator() + childDirPath);
//    qDebug () <<"Size: " <<size <<"\n";
    return size;
}

void FilesManager::SetSelectFlag(int index)
{
 if(SelectStates[index]) SelectStates[index] = false;
 else SelectStates[index] = true;
}

void FilesManager::JumpToSubFolder(QListWidgetItem *item)
{
    if(ui->DataList->currentRow() < ListOffset)
    {
        /*    if(ui->DataList->currentRow() == 0 && item->text() == "..")
        {
            QString TempPrev;
            if(LastPath.size() > 1) TempPrev = LastPath[LastPath.size()-2];
            ListUSBDriveFiles(LastPath.last(), TempPrev);
            LastPath.pop_back();


        }   */
        //  LastPath.push_back(CurrentPath);
        if(IsFilesMode)
            ListUSBDriveFiles(CurrentPath+"/"+item->text());
        else
        {
            QString TempStr;
            if(!item->text().isEmpty()) TempStr = item->text();
            else
            {
                CustomAlbumRow *TempPoint = qobject_cast <CustomAlbumRow*> (item->listWidget()->itemWidget(item));
                if(TempPoint) TempStr = TempPoint->NameLabel.text();

            }
            ListUSBDriveDirectories(CurrentPath+"/"+TempStr);
        }
    }
}

ManagerEngine::ManagerEngine()
{
    USBFile.setFileName("/dev/sda1");
    double TempVal = GetDirectorySize("/home/pi/MusicLibrary")/1000000;
    LibrarySize = TempVal;
    TempVal =  GetDirectorySize("/home/pi/OtherTracks")/1000000;
    LibrarySize += TempVal;
}

void ManagerEngine::DeleteFiles(QStringList *PathList)
{
    for(int a=0; a < PathList->size(); a++)
    {
        MutexPtr->lock();
        emit SendCurrentStatus(QString("%1 z %2").arg(a+1).arg(PathList->size()));
        PlaylistPointer->SearchAndDelete((*PathList)[a]);
        TargetFile.setFileName((*PathList)[a]);
        MutexPtr->unlock();
        TargetFile.remove();    //Mozna pomyslec o dodaniu kontroli bledow
    }
}

void ManagerEngine::DeleteDirectories(QStringList *DirList)
{
    QDir TempDir;
    for(int a=0; a < DirList->size(); a++)
    {
        MutexPtr->lock();
        emit SendCurrentStatus(QString("%1 z %2").arg(a+1).arg(DirList->size()));
        TempDir.setPath((*DirList)[a]);
        MutexPtr->unlock();
        TempDir.removeRecursively();    //Mozna pomyslec o dodaniu kontroli bledow
    }
}

void ManagerEngine::CopyDirectories(QStringList *DirsList)
{
    QDir SourceDir;
    QDir DestDir;
    QFile TempFile;
    QStringList TempFilesList;
    for(int a=0 ; a < DirsList->size(); a++)
    {
        if(LibrarySize > MaxLibrarySize) return;
        MutexPtr->lock();
        emit SendCurrentStatus(QString("%1 z %2").arg(a+1).arg(DirsList->size()));
        SourceDir.setPath((*DirsList)[a]);
        MutexPtr->unlock();
        DestDir.setPath("/home/pi/MusicLibrary/"+SourceDir.dirName());
        if(!DestDir.exists()) DestDir.mkdir(QString("/home/pi/MusicLibrary/"+SourceDir.dirName()));
        TempFilesList = SourceDir.entryList(QStringList() << "*.flac" << "*.mp3" << "*.jpg" << "*.png", QDir::Files);

        for(int x=0; x < TempFilesList.size(); x++)
        {
            //qDebug () <<"Source: " <<QString(SourceDir.path()+"/"+TempFilesList[x]);
            //qDebug () <<"Destination: " <<QString("/home/pi/MusicLibrary/"+SourceDir.dirName()+"/"+TempFilesList[x]);
            TempFile.copy(QString(SourceDir.path()+"/"+TempFilesList[x]), QString("/home/pi/MusicLibrary/"+SourceDir.dirName()+"/"+TempFilesList[x])); //Mozna pomyslec o obsludze bledow
        }

        TempFilesList.clear();
        double TempVal = GetDirectorySize("/home/pi/MusicLibrary")/1000000;
        LibrarySize = TempVal;
        TempVal =  GetDirectorySize("/home/pi/OtherTracks")/1000000;
        LibrarySize += TempVal;
    }
}

void ManagerEngine::CopyFiles(QStringList *PathList)
{
    QFile TempFile;
    QFileInfo TempInfo;
   // qDebug () <<"Copy files!";
    for(int a=0 ; a < PathList->size(); a++)
    {
        if(LibrarySize > MaxLibrarySize+250) return;
       // qDebug () <<"Copy files!";
        MutexPtr->lock();
        emit SendCurrentStatus(QString("%1 z %2").arg(a+1).arg(PathList->size()));
       // qDebug () <<"Copy files!1";
        TempInfo.setFile((*PathList)[a]);
        //qDebug () <<"Copy files!2";
       // qDebug () <<"Source: " <<(*PathList)[a];
        //qDebug () <<"Target: " <<QString("/home/pi/OtherTracks/"+TempInfo.completeBaseName());
        if(!TempFile.copy((*PathList)[a], QString("/home/pi/OtherTracks/"+TempInfo.fileName()))) qDebug () <<"Nie pyklo :(";
        MutexPtr->unlock();
        //qDebug () <<"Copy files!3";
        double TempVal = GetDirectorySize("/home/pi/MusicLibrary")/1000000;
        LibrarySize = TempVal;
        TempVal =  GetDirectorySize("/home/pi/OtherTracks")/1000000;
        LibrarySize += TempVal;
    }
}

void ManagerEngine::SetPointer(playlist *point, QStringList *Files, QStringList *Dirs, int *Mode, QMutex *MutexP)
{
    PlaylistPointer = point;
    FilesLisPtr = Files;
    DirsListPtr = Dirs;
    ModePtr = Mode;
    MutexPtr = MutexP;
    emit SendCurrentLibrarySize(LibrarySize);

}

void FilesManager::USBScan()
{
    if(USBFile.exists() && !(USBLastState))
    {
        emit USBAvailability(true);
       USBLastState = true;
    }
    else if(!(USBFile.exists()) && USBLastState)
    {
        emit USBAvailability(false);
        USBLastState = false;
    }
    QTimer::singleShot(USBRefreshTime, this, SLOT(USBScan()));
}

void ManagerEngine::StartDeleteProcedure(QStringList *PathsList, QStringList *DirList)
{
    DeleteFiles(PathsList);
    if(!DirList->empty()) DeleteDirectories(DirList);
    double TempVal = GetDirectorySize("/home/pi/MusicLibrary")/1000000;
    LibrarySize = TempVal;
    TempVal =  GetDirectorySize("/home/pi/OtherTracks")/1000000;
    LibrarySize += TempVal;
}

void ManagerEngine::StartCopyProcedure(QStringList *PathsList, QStringList *DirList)
{
    if(!PathsList->isEmpty()) CopyFiles(PathsList);
    if(!DirList->isEmpty()) CopyDirectories(DirList);
}

void ManagerEngine::StartProcess()
{
    qDebug () <<"Start process";
    if(*ModePtr == CpFiles || *ModePtr == CpDirectories) StartCopyProcedure(FilesLisPtr, DirsListPtr);
    else if(*ModePtr == DelDirectories || *ModePtr == DelFiles) StartDeleteProcedure(FilesLisPtr, DirsListPtr);

    emit SendCurrentLibrarySize(LibrarySize);
    emit Finish();
}

void FilesManager::on_BackButton_clicked()
{
    if(ui->stackedWidget->currentIndex() == 0)
    {
        close();
        if(IsAlreadyMounted && !(EngineThread.isRunning() && (SharedMode == CpFiles || SharedMode == CpDirectories)))
        {
            DevProcess.execute("sudo umount /mnt");
            IsAlreadyMounted = false;
        }
    }
    else ui->stackedWidget->setCurrentIndex(0);
    disconnect(ui->DataList, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(JumpToSubFolder(QListWidgetItem*)));
}

void FilesManager::on_AddTracksButton_clicked()
{
    if(!USBLastState) return;
    SharedMode = CpFiles;
    if(!IsAlreadyMounted)
    {
        DevProcess.execute("sudo mount /dev/sda1 /mnt");
        IsAlreadyMounted = true;
    }
    ListUSBDriveFiles("/mnt");
    ui->stackedWidget->setCurrentIndex(1);
    IsFilesMode = true;
    connect(ui->DataList, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(JumpToSubFolder(QListWidgetItem*)));
}

void FilesManager::on_AddAlbumsButton_clicked()
{
    if(!USBLastState) return;
    SharedMode = CpDirectories;
    if(!IsAlreadyMounted)
    {
        DevProcess.execute("sudo mount /dev/sda1 /mnt");
        IsAlreadyMounted = true;
    }
    ListUSBDriveDirectories("/mnt");
    ui->stackedWidget->setCurrentIndex(1);
    IsFilesMode = false;
    connect(ui->DataList, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(JumpToSubFolder(QListWidgetItem*)));
}

void FilesManager::on_ExecuteButton_clicked()
{
    if(!EngineThread.isRunning())
    {
    FilesToExecute.clear();
    DirsToExecute.clear();
    }
    for(int x=0; x < SelectStates.size(); x++)
    {
        //if(SelectStates[x]) qDebug () <<CurrentPath+"/"+CurrentFilesList[x];
        if(SelectStates[x])
        {
            switch(SharedMode)
            {
            case CpFiles:
                SharedMutex.lock();
                FilesToExecute.push_back(CurrentPath+"/"+CurrentFilesList[x]);
                SharedMutex.unlock();
                ui->AddAlbumsButton->setEnabled(false);
                ui->DelAlbumsButton->setEnabled(false);
                ui->DelTracksButton->setEnabled(false);
                break;

            case CpDirectories:
                SharedMutex.lock();
                DirsToExecute.push_back(CurrentPath+"/"+CurrentFilesList[x]);
                SharedMutex.unlock();
                ui->AddTracksButton->setEnabled(false);
                ui->DelAlbumsButton->setEnabled(false);
                ui->DelTracksButton->setEnabled(false);
                break;

            case DelFiles:
                SharedMutex.lock();
                FilesToExecute.push_back(CurrentPath+"/"+CurrentFilesList[x]);
                SharedMutex.unlock();
                ui->AddAlbumsButton->setEnabled(false);
                ui->AddTracksButton->setEnabled(false);
                ui->DelAlbumsButton->setEnabled(false);
                break;

            case DelDirectories:
                QDir TempDir(CurrentPath+"/"+CurrentFilesList[x]);
                QStringList TempData = TempDir.entryList(QStringList() << "*.flac" << "*.mp3", QDir::Files);
                for(int z=0; z < TempData.size(); z++)
                {
                    SharedMutex.lock();
                    FilesToExecute.push_back(QString(CurrentPath+"/"+CurrentFilesList[x]+"/"+TempData[z]));
                    SharedMutex.unlock();
                }
                //FilesToExecute += ;
                SharedMutex.lock();
                DirsToExecute.push_back(CurrentPath+"/"+CurrentFilesList[x]);
                SharedMutex.unlock();
                ui->AddAlbumsButton->setEnabled(false);
                ui->AddTracksButton->setEnabled(false);
                ui->DelTracksButton->setEnabled(false);
                break;
            }
        }
    }
    EngineThread.start();
}

void FilesManager::on_DelAlbumsButton_clicked()
{
    SharedMode = DelDirectories;
    ListUSBDriveDirectories("/home/pi/MusicLibrary", true);
    ui->stackedWidget->setCurrentIndex(1);
    IsFilesMode = false;
}

void FilesManager::on_DelTracksButton_clicked()
{
    SharedMode = DelFiles;
    ListUSBDriveFiles("/home/pi/OtherTracks", true);
    ui->stackedWidget->setCurrentIndex(1);
    IsFilesMode = true;
}

void FilesManager::ProcFinished()
{
    ui->AddAlbumsButton->setEnabled(true);
    ui->AddTracksButton->setEnabled(true);
    ui->DelAlbumsButton->setEnabled(true);
    ui->DelTracksButton->setEnabled(true);
    ui->InfoLabel->setText("");
    if(SharedMode != CpFiles || SharedMode != DelFiles) emit RefreshLibrary();
    if(ui->stackedWidget->currentIndex() != 1 && (SharedMode == CpFiles || SharedMode == CpDirectories))
    {
        IsAlreadyMounted = false;
        DevProcess.execute("sudo umount /mnt");
    }
    if(SharedMode == CpFiles || SharedMode == CpDirectories) FinishBox.setText("Zakonczono kopiowanie plikow!");
    else FinishBox.setText("Zakonczono usuwanie plikow!");
    FinishBox.show();
}

void FilesManager::GetCurrentStatus(QString Status)
{
    switch(SharedMode)
    {
    case CpFiles:
        ui->InfoLabel->setText(QString("Kopiowanie plikow: %1").arg(Status));
        break;
    case CpDirectories:
        ui->InfoLabel->setText(QString("Kopiowanie albumow: %1").arg(Status));
        break;
    case DelFiles:
        ui->InfoLabel->setText(QString("Usuwanie albumow: %1").arg(Status));
        break;
    case DelDirectories:
        ui->InfoLabel->setText(QString("Usuwanie plikow: %1").arg(Status));
        break;

    }
}

void FilesManager::SetLibrarySizeIndicator(unsigned int CurrentSize)
{
    int Val;
    if(CurrentSize > MaxLibrarySize) Val = 100;
    else Val = (CurrentSize*100)/MaxLibrarySize;
    ui->LibrarySizeBar->setValue(Val);
    //qDebug () <<"Library size: " <<CurrentSize;
}
