#ifndef LIGHTSETTINGS_H
#define LIGHTSETTINGS_H

#include <QDialog>
#include <QFile>
#include <QTextStream>
#include <ledcontrol.h>

namespace Ui {
class LightSettings;
}

class LightSettings : public QDialog
{
    Q_OBJECT

public:
    explicit LightSettings(QWidget *parent = 0);
    ~LightSettings();
    void SaveSettings();
    void LoadSettings();
    void SwitchLamp();
    void SetPointer(LedControl *Pointer);
    void RefreshWindow();

private slots:
    void on_BackButton_clicked();

    void on_MinusHue_clicked();

    void on_PlusHue_clicked();

    void on_MinusSaturation_clicked();

    void on_PlusSaturation_clicked();

    void on_MinusValue_clicked();

    void on_PlusValue_clicked();

    void on_ApplyButton_clicked();

private:
    Ui::LightSettings *ui;
    int HVal = 255, SVal = 255, VVal = 255;
    QColor CurrentColor;
    QPalette CurrentPalette;
    void SetPreviewColor();
    QFile SettingsFile;
    LedControl *LedPointer = nullptr;
    bool IsEnabledLamp = false;
    bool IsSavedChanges = true;
};

#endif // LIGHTSETTINGS_H
