#include "alarmset.h"

AlarmSet::AlarmSet(QObject *parent) : QObject(parent)
{
Time.setHMS(7,0,0);
Date = QDateTime::currentDateTime().date();
for(int i=0; i < 7; i++)
{
    Days[i]=false;
}
}


void AlarmSet::SetEnabled(bool Val)
{
    IsEnabled = Val;
}
void AlarmSet::SetAsOnce(bool Val)
{
    IsOnce = Val;
}
void AlarmSet::SetDays(int Day, bool Val)
{
    Days[Day] = Val;
}
void AlarmSet::SetHour(int Hour, int minute)
{
   Time.setHMS(Hour, minute, 0);
}
void AlarmSet::SetOnceDate(int Day, int Month, int Year)
{
    Date.setDate(Year, Month, Day);
}

void AlarmSet::SetSecs(int Val)
{
    Time.setHMS(Time.hour(), Time.minute(), Val);
}

void AlarmSet::SetAlarmMode(int Val)
{
    AlarmMode = Val;
}

void AlarmSet::SetAlarmValue(QString Val)
{
    AlarmValue = Val;
}

void AlarmSet::SetSunriseEnabled(bool Val)
{
    IsSunriseEnabled = Val;
}

void AlarmSet::SetSunriseTime(int Val)
{
    SunriseTime = Val;
}

bool AlarmSet::GetEnabledStatus()
{
    return IsEnabled;
}
bool AlarmSet::GetOnceStatus()
{
    return IsOnce;
}
bool AlarmSet::GetDayState(int Day)
{
    return Days[Day];
}
QTime AlarmSet::GetTime()
{
    return Time;
}
QDate AlarmSet::GetOnceDate()
{
    return Date;
}

int AlarmSet::GetAlarmMode()
{
    return AlarmMode;
}

QString AlarmSet::GetAlarmValue()
{
    //if(AlarmMode == DefaultSignal || AlarmMode == FunStart) return "";
    return AlarmValue;
}

bool AlarmSet::GetIsSunriseEnabled()
{
    return IsSunriseEnabled;
}

int AlarmSet::GetSunriseTime()
{
    return SunriseTime;
}
